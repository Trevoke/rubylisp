(describe frame-syntax
          (check '[1 2] '(make-vector 1 2)))

(describe frame-rendering
          (check (str (make-frame a: 1)) "{a: 1}"))

(describe naked-symbols
          (check a: 'a:))

(describe frame-access
          (check (get-slot {a: 1 b: 2 c: 3} a:) 1)
          (check (get-slot {a: 1 b: 2 c: 3} b:) 2))

(describe frame-mutation
          (let ((f {a: 1 b: 2 c: 3}))
            (check (set-slot! f a: 5) 5)
            (check (get-slot f a:) 5)))

(describe frame-method
          (let ((f {a: 5
                    b: 2
                    foo: (lambda (x)
                           (+ x a))}))
            (check (send f foo: 1) 6))
          (let ((f {a: 5
                    b: 2
                    foo: (lambda (x)
                           (set! b (+ x a)))}))
            (check (send f foo: 1) 6)
            (check (get-slot f b:) 6)))

(describe prototypes
          (let* ((f {a: 2
                     b: 1})
                 (g {parent*: f
                     a: 3}))
            (check (get-slot g b:) 1)
            (check (get-slot g a:) 3)
            (set-slot! g a: 1)
            (check (get-slot g a:) 1)
            (set-slot! g b: 10)
            (check (get-slot g b:) 10))
          (let* ((adder {add: (lambda (x)
                                (+ x a))})
                 (incrementor {parent*: adder
                               a: 1}))
            (check (send incrementor add: 3) 4)))

(describe new-slots
          (let ((f {a: 1}))
            (check (set-slot! f b: 5) 5)
            (check (get-slot f b:) 5)))

(describe function-slot-use
          (let ((f {a: 5
                    b: 2
                    foo: (lambda (x)
                           (+ x a))
                    bar: (lambda ()
                           (foo b))}))
            (check (send f bar:) 7)))

(describe inherited-function-slot-use
          (let* ((f {a: 5
                     foo: (lambda (x)
                            (+ x a))})
                 (g {parent*: f
                     b: 2
                     bar: (lambda ()
                            (foo b))}))
            (check (send g bar:) 7)))

(describe multiple-parents
          (let* ((e {a: 5})
                 (f {b: 2})
                 (g {parent-e*: e
                     parent-f*: f
                     foo: (lambda (x)
                            (+ x a))
                     bar: (lambda ()
                            (foo b))}))
            (check (send g bar:) 7)
            (set-slot! g a: 10)
            (check (get-slot g a:) 10)
            (check (get-slot e a:) 5)))

(describe calling-super
          (let* ((f {foo: (lambda () 42)})
                 (g {parent*: f  foo: (lambda () (+ 1 (send-super foo:)))}))
            (check (send g foo:) 43)))

(describe locals-override-slots
          (let* ((f {a: 42})
                 (g {parent*: f  foo: (lambda ()
                                        (let ((a 10))
                                          (+ 1 a)))}))
            (check (send g foo:) 11)))

(describe cloning
          (let* ((f {a: 1 b: 2})
                 (g (clone f)))
            (check (equal? f g) #t)
            (set-slot! f a: 42)
            (check (get-slot f a:) 42)
            (check (get-slot g a:) 1)))

(describe has-slot
          (let ((f {a: 1 b: 2}))
            (check (has-slot? f a:) #t)
            (check (has-slot? f b:) #t)
            (check (has-slot? f c:) #f)))

(describe remove-slots
          (let* ((e {a: 5})
                 (f {b: 2})
                 (g {parent-e*: e
                     parent-f*: f
                     foo: 3
                     bar: 10}))
            (check (remove-slot! g foo:) #t)
            (check (has-slot? g foo:) #f)
            (check (remove-slot! g a:) #f)
            (check (has-slot? e a:) #t)))

(describe keys
          (let ((f {a: 1 b: 2}))
            (check (keys f) '(a: b:))))

(describe shortcuts
          (let ((f {a: 1 b: 2}))
            (check (a:? f) #t)
            (check (c:? f) #f)
            (check (a: f) 1)
            (check (b: f) 2)
            (a:! f 42)
            (check (a: f) 42)))

