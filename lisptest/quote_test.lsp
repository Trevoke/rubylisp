(describe quote
          (check* (quote 1) 1)
          (check* (quote (1 2 3)) (1 2 3))
          (check* (quote (+ 1 2)) (+ 1 2)))
