(describe reverse
          (check (reverse '(a)) '(a))
          (check (reverse '(a b)) '(b a))
          (check (reverse '(a b c d)) '(d c b a)))

(describe map
          (check (map (lambda (x) (+ x 1)) '(1 2 3 4)) '(2 3 4 5))
          (check (map + '(1 2 3 4) '(1 1 1 1)) '(2 3 4 5))
          (check (map (lambda (x) (list x x)) '(a b c)) '((a a) (b b) (c c))))

(describe reduce
          (check (reduce-left + 0 '(1 2 3 4)) 10)
          (check (reduce-left + 0 '()) 0)
          (check (reduce-left + 0 '(1)) 1)
          (check (reduce-left * 1 '(2 3 4)) 24)
          (check (reduce-left list '() '(1 2 3 4)) '(((1 2) 3) 4))
          (check (reduce-left list '() '()) '())
          (check (reduce-left list '() '(1)) '1)
          (check (reduce-left (lambda (acc x) (list acc x)) '() '(1 2 3 4)) '(((1 2) 3) 4)))

(describe append
          (define a '(1 2 3))
          (define b '(4 5 6))
          (check (append '(1 2 3) '(4 5 6)) '(1 2 3 4 5 6))
          (check (append a b) '(1 2 3 4 5 6))
          (check a '(1 2 3))
          (check b '(4 5 6))
          (check (append '(1 2) '(3 4) '(5 6)) '(1 2 3 4 5 6)))

(describe append!
          (define a '(1 2 3))
          (define b '(4 5 6))
          (define c '(7 8 9))
          (check (append! '(1 2 3) '(4 5 6)) '(1 2 3 4 5 6))
          (check (append! a b c) '(1 2 3 4 5 6 7 8 9))
          (check a '(1 2 3 4 5 6 7 8 9))
          (check b '(4 5 6 7 8 9))
          (check c '(7 8 9)))

(describe filter
          (check (filter odd? '(1 2 3 4 5 6)) '(1 3 5)))

(describe remove
          (check (remove odd? '(1 2 3 4 5 6)) '(2 4 6)))

(describe partition
          (check (partition odd? '(1 2 3 4 5 6)) '((1 3 5) (2 4 6))))

(describe searching
          (check (memq 'a '(a b c)) '(a b c))
          (check (memq 'b '(a b c)) '(b c))
          (check (memq 'a '(b c d)) #f)
          (check (memq '(a) '(b (a) c)) #f)
          (check (member (list 'a) '(b (a) c)) '((a) c))
          (check (memv 101 '(100 101 102)) '(101 102)))

(describe any
          (check (any integer? '(a 3 b 2.7)))
          (check! (any integer? '(a 3.1 b 2.7)))
          (check (any < '(3 1 4 1 5)
                        '(2 7 1 8 2))))

(describe every
          (check (every integer? '(4 3 8 27)))
          (check! (every integer? '(a 3 b 7)))
          (check (every < '(3 1 4 1 0)
                          '(5 7 9 8 2))))
