// Test list access primitives

(define l '(1 2 3 4 5 6 7 8 9 10))

(describe list
          (check (list 'a) '(a))
          (check (list (+ 1 1) (+ 1 2)) '(2 3)))

(describe length
          (check (length nil) 0)
          (check (length '()) 0)
          (check (length '(1)) 1)
          (check (length '(1 2)) 2)
          (check (length l) 10))

(describe first
          (check (first '(1 2 3 4 5 6 7 8 9 10)) 1)
          (check (first l) 1))

(describe second
          (check (second '(1 2 3 4 5 6 7 8 9 10)) 2)
          (check (second l) 2))

(describe third
          (check (third '(1 2 3 4 5 6 7 8 9 10)) 3)
          (check (third l) 3))

(describe fourth
          (check (fourth '(1 2 3 4 5 6 7 8 9 10)) 4)
          (check (fourth l) 4))

(describe fifth
          (check (fifth '(1 2 3 4 5 6 7 8 9 10)) 5)
          (check (fifth l) 5))

(describe sixth
          (check (sixth '(1 2 3 4 5 6 7 8 9 10)) 6)
          (check (sixth l) 6))

(describe seventh
          (check (seventh '(1 2 3 4 5 6 7 8 9 10)) 7)
          (check (seventh l) 7))

(describe eighth
          (check (eighth '(1 2 3 4 5 6 7 8 9 10)) 8)
          (check (eighth l) 8))

(describe ninth
          (check (ninth '(1 2 3 4 5 6 7 8 9 10)) 9)
          (check (ninth l) 9))

(describe tenth
          (check (tenth '(1 2 3 4 5 6 7 8 9 10)) 10)
          (check (tenth l) 10))

(describe nth
          (check (nth 1 l) 1)
          (check (nth 2 l) 2)
          (check (nth 3 l) 3)
          (check (nth 4 l) 4)
          (check (nth 5 l) 5)
          (check (nth 6 l) 6)
          (check (nth 7 l) 7)
          (check (nth 8 l) 8)
          (check (nth 9 l) 9)
          (check (nth 10 l) 10))

(describe car
          (check (car nil) nil)
          (check (car '(1)) 1)
          (check (car l) 1))

(describe cdr
          (check (cdr nil) nil)
          (check (cdr '(1)) nil)
          (check (length (cdr l)) 9))

(describe rest
          (check (rest nil) nil)
          (check (rest '(1)) nil)
          (check (rest '(1 2 3)) '(2 3)))

(describe caar
          (check (caar nil) nil)
          (check (caar '((1))) 1))

(describe cadr
          (check (cadr nil) nil)
          (check (cadr '(1 2)) 2))

(describe cdar
          (check (cdar nil) nil)
          (check (cdar '(1)) nil)
          (check (cdar '((1 2) 3)) '(2)))

(describe cddr
          (check (cddr nil) nil)
          (check (cddr '(1)) nil)
          (check (cddr '(1 2 3)) '(3)))

(describe sublist
          (check (sublist '(a b c d e f) 2 5) '(b c d e)))

(describe list-head
          (check (list-head '(a b c d e f) 3) '(a b c)))

(describe take
          (check (take 3 '(a b c d e f)) '(a b c)))

(describe list-tail
          (check (list-tail '(a b c d e f) 3) '(d e f)))

(describe drop
          (check (drop 3 '(a b c d e f)) '(d e f)))

(describe last-pair
          (check (last-pair '(a b c d)) '(d))
          (check (last-pair '(a b c . d)) '(c . d)))

