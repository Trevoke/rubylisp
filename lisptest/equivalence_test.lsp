(define xx 2)

(define (f) 42)
(define (g) 42)

(define frame1 {a: 1})

(define l1 '(a b c))
(define l1 l2)

(define s1 "hi")
(define s2 s1)

(describe integer-equal-true
          (check (== xx xx))
          (check (== 2 xx))
          (check (= xx xx))
          (check (= 2 xx)))

(describe integer-equal-false
          (check! (== 2 3))
          (check! (== 2 2.0))
          (check! (= 2 3))
          (check! (= 2 2.0)))

(describe integer-not-equal-true
          (check (/= 2 3))
          (check (/= 2 2.0))
          (check (!= 2 3))
          (check (!= 2 2.0)))

(describe integer-not-equal-false
          (check! (/= xx xx))
          (check! (/= 2 xx))
          (check! (!= xx xx))
          (check! (!= 2 xx)))


(describe eqv?-true
          (check (eqv? #t #t))
          (check (eqv? #f #f))
          (check (eqv? 'a 'a))
          (check (eqv? 2 2))
          (check (eqv? 2.5 2.5))
          (check (eqv? '() '()))
          (check (eqv? nil nil))
          (check (eqv? f f))
          (check (eqv? l1 l2))
          (check (eqv? s1 s2))
          (check (eqv? frame1 frame1)))

(describe eqv?-false
          (check! (eqv? #t #f))
          (check! (eqv? 'a 'b))
          (check! (eqv? 2 3))
          (check! (eqv? 2.5 3.7))
          (check! (eqv? 2 2.0))
          (check! (eqv? '() '(1)))
          (check! (eqv? f g))
          (check! (eqv? l1 '(a b c)))
          (check! (eqv? "hi" "hi"))
          (check! (eqv? {a: 1} {a: 1})))

(describe equal?-true
          (check (equal? 'a 'a))
          (check (equal? '(a) '(a)))
          (check (equal? '(a (b) c)
                          '(a (b) c)))
          (check (equal? "abc" "abc"))
          (check (equal? 2 2))
          (check (equal? {a: 1} {a: 1})))
