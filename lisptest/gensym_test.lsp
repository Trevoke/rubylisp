(describe gensym
          (check (gensym) 'GENSYM-0)
          (check (gensym) 'GENSYM-1)
          (check (gensym "test") 'test-2)
          (check (gensym "test") 'test-3)
          (check (gensym) 'GENSYM-4)
          )
