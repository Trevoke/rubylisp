(describe acons
          (check (alist-to-list (acons 'a 1)) '((a . 1)))
          (check (alist-to-list (acons 'a 1  (acons 'b 2))) '((b . 2) (a . 1))))

(describe zip
          (check (alist-to-list (zip '(a b c) '(1 2 3))) '((a . 1) (b . 2) (c . 3))))

(describe assoc
          (check (assoc 'a (list-to-alist '((a . 1) (b . 2)))) '(a . 1))
          (check (assoc 'b (list-to-alist '((a . 1) (b . 2)))) '(b . 2)))

(describe rassoc
          (check (rassoc 1 (list-to-alist '((a . 1) (b . 2)))) '(a . 1))
          (check (rassoc 2 (list-to-alist '((a . 1) (b . 2)))) '(b . 2)))

(describe dissoc
          (check (alist-to-list (dissoc 'a (list-to-alist '((a . 1) (b . 2))))) '((b . 2)))
          (check (alist-to-list (dissoc 'b (list-to-alist'((a . 1) (b . 2))))) '((a . 1))))
 
