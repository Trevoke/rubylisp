(describe character-lookup
          (check (char->name #\U+0A) "linefeed")
          (check (name->char "BEL") #\U+07))

(describe character-comparison
          (check (char=? #\BEL #\U+07))
          (check! (char=? #\BEL #\U+08))

          (check (char<? #\U+10 #\U+11))
          (check! (char<? #\U+11 #\U+10))

          (check (char>? #\U+11 #\U+10))
          (check! (char>? #\U+10 #\U+10))

          (check (char<=? #\U+10 #\U+11))
          (check (char<=? #\U+10 #\U+10))
          (check! (char<=? #\U+11 #\U+10))

          (check (char>=? #\U+11 #\U+10))
          (check (char>=? #\U+10 #\U+10))
          (check! (char>=? #\U+10 #\U+11)))

(describe character-case-independant-comparison
          (check (char-ci=? #\a #\a))
          (check (char-ci=? #\a #\A))
          (check! (char-ci=? #\a #\b))
          (check! (char-ci=? #\a #\B))

          (check (char-ci<? #\A #\B))
          (check (char-ci<? #\a #\B))
          (check! (char-ci<? #\b #\a))
          (check! (char-ci<? #\b #\A))

          (check (char-ci>? #\B #\A))
          (check (char-ci>? #\B #\a))
          (check! (char-ci>? #\a #\b))
          (check! (char-ci>? #\A #\b))

          (check (char-ci<=? #\A #\B))
          (check (char-ci<=? #\a #\B))
          (check! (char-ci<=? #\b #\a))
          (check! (char-ci<=? #\b #\A))
          (check (char-ci<=? #\b #\B))
          (check (char-ci<=? #\b #\b))

          (check (char-ci>=? #\B #\A))
          (check (char-ci>=? #\B #\a))
          (check! (char-ci>=? #\a #\b))
          (check! (char-ci>=? #\A #\b))
          (check (char-ci>=? #\a #\a))
          (check (char-ci>=? #\A #\a)))

(describe upcase
          (check (char-upcase #\a) #\A)
          (check (char-upcase #\A) #\A)
          (check (char-upcase #\BEL) #\BEL))

(describe downcase
          (check (char-downcase #\A) #\a)
          (check (char-downcase #\a) #\a)
          (check (char-downcase #\BEL) #\BEL))

(describe char->digit-bad-chars
          (check! (char->digit #\@))
          (check! (char->digit #\BEL)))

(describe char->digit
          (check (char->digit #\0) 0)
          (check (char->digit #\1) 1)
          (check (char->digit #\2) 2)
          (check (char->digit #\3) 3)
          (check (char->digit #\4) 4)
          (check (char->digit #\5) 5)
          (check (char->digit #\6) 6)
          (check (char->digit #\7) 7)
          (check (char->digit #\8) 8)
          (check (char->digit #\9) 9)
          (check (char->digit #\A) #f))

(describe char->digit-base-10
          (check (char->digit #\0 10) 0)
          (check (char->digit #\1 10) 1)
          (check (char->digit #\2 10) 2)
          (check (char->digit #\3 10) 3)
          (check (char->digit #\4 10) 4)
          (check (char->digit #\5 10) 5)
          (check (char->digit #\6 10) 6)
          (check (char->digit #\7 10) 7)
          (check (char->digit #\8 10) 8)
          (check (char->digit #\9 10) 9)
          (check (char->digit #\A 10) #f))

(describe char->digit-base-8
          (check (char->digit #\0 8) 0)
          (check (char->digit #\1 8) 1)
          (check (char->digit #\2 8) 2)
          (check (char->digit #\3 8) 3)
          (check (char->digit #\4 8) 4)
          (check (char->digit #\5 8) 5)
          (check (char->digit #\6 8) 6)
          (check (char->digit #\7 8) 7)
          (check (char->digit #\8 8) #f))

(describe char->digit-base-16
          (check (char->digit #\0 16) 0)
          (check (char->digit #\1 16) 1)
          (check (char->digit #\2 16) 2)
          (check (char->digit #\3 16) 3)
          (check (char->digit #\A 16) 10)
          (check (char->digit #\a 16) 10)
          (check (char->digit #\f 16) 15)
          (check (char->digit #\g 16) #f))


(describe digit->char-bad-chars
          (check! (digit->char 50))
          (check! (digit->char -2)))

(describe digit->char
          (check (digit->char 0) #\0)
          (check (digit->char 1) #\1)
          (check (digit->char 2) #\2)
          (check (digit->char 3) #\3)
          (check (digit->char 4) #\4)
          (check (digit->char 5) #\5)
          (check (digit->char 6) #\6)
          (check (digit->char 7) #\7)
          (check (digit->char 8) #\8)
          (check (digit->char 9) #\9))

(describe digit->char-base-10
          (check (digit->char 0 10) #\0)
          (check (digit->char 1 10) #\1)
          (check (digit->char 2 10) #\2)
          (check (digit->char 3 10) #\3)
          (check (digit->char 4 10) #\4)
          (check (digit->char 5 10) #\5)
          (check (digit->char 6 10) #\6)
          (check (digit->char 7 10) #\7)
          (check (digit->char 8 10) #\8)
          (check (digit->char 9 10) #\9))

(describe digit->char-base-8
          (check (digit->char 0 8) #\0)
          (check (digit->char 1 8) #\1)
          (check (digit->char 2 8) #\2)
          (check (digit->char 3 8) #\3)
          (check (digit->char 4 8) #\4)
          (check (digit->char 5 8) #\5)
          (check (digit->char 6 8) #\6)
          (check (digit->char 7 8) #\7))

(describe digit->char-base-16
          (check (digit->char 0 16) #\0)
          (check (digit->char 1 16) #\1)
          (check (digit->char 2 16) #\2)
          (check (digit->char 3 16) #\3)
          (check (digit->char 10 16) #\A)
          (check (digit->char 15 16) #\F))

(describe char->int
          (check (char->integer #\0) 48)
          (check (char->integer #\[) 91)
          (check (char->integer #\q) 113))

(describe int->char
          (check (integer->char 48) #\0)
          (check (integer->char 91) #\[)
          (check (integer->char 113) #\q))
