(define fx 2.2)
(define fy 8.8)
(define fz 7.7)

(describe arithmetic-test
    (check (+ 5.0 5.0) 10.0)
    (check (- 10.0 7.0) 3.0)
    (check (* 2.0 4.0)  8.0)
    (check (/ 25.0 5.0) 5.0))

(describe condition-test
    (check (< fx fy)  #t)
    (check (< fy fz)  #f)
    (check (> fx fy)  #f)
    (check (> fz fx)  #t)
    (check (<= fx 2.2) #t)
    (check (>= fz 7) #t))
