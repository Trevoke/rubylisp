(describe quasiquoted-literal
          (check `a 'a)
          (check `1 1))

(describe quasiquoted-list
          (check `(a b c) '(a b c))
          (check `(1 (2) 3) '(1 (2) 3)))

(describe unquote
          (check `(a ,(+ 1 2) b) '(a 3 b)))

(describe unquote-splicing
          (check `(a ,@(list 1 2 3) b) '(a 1 2 3 b)))

(describe combined-and-eval
          (let ((x 1)
                (y '(2 3)))
            (check (eval `(+ ,x ,@y)) 6)))

(describe defmacro
          (defmacro (add x y)
            `(+ ,x ,@y))
          (check (add 1 (2 3)) 6))

(describe defmacro-with-varargs
          (defmacro (add x . y)
            `(+ ,x ,@y))
          (check (add 1 2 3) 6))

(describe expand
          (defmacro (add x . y)
            `(+ ,x ,@y))
          (check (expand add 1 2 3) '(+ 1 2 3)))

(describe nested
          (check  `(a `(b ,(+ 1 2) ,(foo ,(+ 1 3) d) e) f) '(a `(b ,(+ 1 2) ,(foo 4 d) e) f)))
