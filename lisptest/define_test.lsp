(describe defining-a-variable
          (check (begin (define a 1)
                        a)
                 1)
          (check (begin (define a (+ 1 2))
                        a)
                 3))


(describe defining-a-function-and-calling-with-simple-args
          (define (add a b) (+ a b))
          (check (add 1 2) 3))

(describe defining-a-function-and-calling-with-complex-args
          (define (add a b) (+ a b))
          (check (add (* 2 2) (+ 3 4)) 11))

(describe functions-referencing-outer-environment
          (define x 4)
          (define (plus-x b) (+ b x))
          (check (plus-x 2) 6)
          (check (plus-x 10) 14))

(describe closures
          (define x 10)
          (define (outer x)
            (define (inner y)
              (+ x y))
            inner)
          (define f1 (outer 1))
          (define f2 (outer 2))
          (check (f1 2) 3)
          (check (f2 2) 4))

(describe lambda
          (define f (lambda (x y) (+ x y)))
          (check (f 1 2) 3)
          (check ((lambda (x y) (+ x y)) 1 2) 3)
          (check (apply (lambda (x y) (+ x y)) 1 2) 3))

(define (bar x)
  (if (== x 0)
      0
    (+ x (bar (- x 1)))))

(describe recursive-function
          (check (bar 0) 0)
          (check (bar 1) 1)
          (check (bar 2) 3)
          (check (bar 3) 6)
          (check (bar 4) 10))

(define (f a b . c)
           (check a 1)
           (check b 2)
           (check c '(3 4 5)))

(describe var-args
          (f 1 2 3 4 5))
