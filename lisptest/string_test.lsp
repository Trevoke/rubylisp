(describe string?
          (check (string? "bar"))
          (check! (string? 3)))


(describe str
          (check (str "hello") "hello")
          (check (str 1) "1")
          (check (str #t) "#t")
          (check (str 'a) "a")
          (check (str '(1 2 3)) "(1 2 3)")
          (check (str 1 "hello" "-" '(1 2 3)) "1hello-(1 2 3)"))

(describe make-string
          (check (make-string 5 #\a) "aaaaa")
          (check (make-string 5) "     "))

(describe string
          (check (string) "")
          (check (string #\a) "a")
          (check (string #\a #\space #\b) "a b"))

(describe list->string
          (check (list->string '()) "")
          (check (list->string '(#\a)) "a")
          (check (list->string '(#\a #\space #\b)) "a b"))

(describe string->list
          (check (string->list "") '())
          (check (string->list "a") '(#\a))
          (check (string->list "a b") '(#\a #\space #\b)))

(describe string-copy
          (check (string-copy "abc") "abc"))

(describe string-length
          (check (string-length "") 0)
          (check (string-length "a") 1)
          (check (string-length "abcd") 4))

(describe string-null?
          (check (string-null? ""))
          (check! (string-null? "a"))
          (check! (string-null? "abcd")))

(describe string-ref
          (check (string-ref "Hello" 0) #\H)
          (check (string-ref "Hello" 1) #\e)
          (check (string-ref "Hello" 2) #\l)
          (check (string-ref "Hello" 3) #\l)
          (check (string-ref "Hello" 4) #\o)
          (check (string-ref "Hello" 5) #f))

(describe string-set!
          (define s "Dog")
          (string-set! s 0 #\L)
          (check s "Log")
          (check (string-set! s 3 #\t) #f))

(describe string=?
          (check (string=? "PIE" "PIE"))
          (check! (string=? "PIE" "pie")))

(describe string-ci=?
          (check (string-ci=? "PIE" "PIE"))
          (check (string-ci=? "PIE" "pie")))

(describe substring=?
          (check (substring=? "Alamo" 1 3 "cola" 2 4))
          (check! (substring=? "Alamo" 1 3 "COLA" 2 4))
          (check! (substring=? "Alamo" 1 3 "colt" 2 4)))

(describe substring-ci=?
          (check (substring-ci=? "Alamo" 1 3 "cola" 2 4))
          (check (substring-ci=? "Alamo" 1 3 "COLA" 2 4))
          (check! (substring-ci=? "Alamo" 1 3 "colt" 2 4))
          (check! (substring-ci=? "Alamo" 1 3 "COLT" 2 4)))

(describe string<?
          (check (string<? "cat" "dog"))
          (check (string<? "cat" "catkin"))
          (check! (string<? "cat" "DOG"))
          (check! (string<? "cat" "cat")))

(describe string-ci<?
          (check (string-ci<? "cat" "dog"))
          (check (string-ci<? "cat" "catkin"))
          (check (string-ci<? "cat" "DOG"))
          (check! (string-ci<? "cat" "cat")))

(describe substring<?
          (check (substring<? "-cat-" 1 4 "-cat-" 1 5))
          (check (substring<? "-cat-" 1 4 "-dog-" 1 4))
          (check! (substring<? "-cat-" 1 4 "-DOG-" 1 4))
          (check! (substring<? "-cat-" 1 4 "-cat-" 1 4)))

(describe substring-ci<?
          (check (substring-ci<? "-cat-" 1 4 "-cat-" 1 5))
          (check (substring-ci<? "-cat-" 1 4 "-dog-" 1 4))
          (check (substring-ci<? "-cat-" 1 4 "-DOG-" 1 4))
          (check! (substring-ci<? "-cat-" 1 4 "-cat-" 1 4)))

(describe string<=?
          (check (string<=? "cat" "dog"))
          (check (string<=? "cat" "catkin"))
          (check! (string<=? "cat" "DOG"))
          (check (string<=? "cat" "cat")))

(describe string-ci<=?
          (check (string-ci<=? "cat" "dog"))
          (check (string-ci<=? "cat" "catkin"))
          (check (string-ci<=? "cat" "DOG"))
          (check (string-ci<=? "cat" "cat")))

(describe substring<=?
          (check (substring<=? "-cat-" 1 4 "-cat-" 1 5))
          (check (substring<=? "-cat-" 1 4 "-dog-" 1 4))
          (check! (substring<=? "-cat-" 1 4 "-DOG-" 1 4))
          (check (substring<=? "-cat-" 1 4 "-cat-" 1 4)))

(describe substring-ci<=?
          (check (substring-ci<=? "-cat-" 1 4 "-cat-" 1 5))
          (check (substring-ci<=? "-cat-" 1 4 "-dog-" 1 4))
          (check (substring-ci<=? "-cat-" 1 4 "-DOG-" 1 4))
          (check (substring-ci<=? "-CAT-" 1 4 "-cat-" 1 4)))

(describe string>=?
          (check (string>=? "dog" "cat"))
          (check (string>=? "catkin" "cat"))
          (check! (string>=? "DOG" "cat"))
          (check (string>=? "cat" "cat")))

(describe string-ci>=?
          (check (string-ci>=? "dog" "cat"))
          (check (string-ci>=? "catkin" "cat"))
          (check (string-ci>=? "DOG" "cat"))
          (check (string-ci>=? "cat" "cat")))

(describe substring>=?
          (check (substring>=? "-cat-" 1 5 "-cat-" 1 4))
          (check (substring>=? "-dog-" 1 4 "-cat-" 1 4))
          (check! (substring>=? "-DOG-" 1 4 "-cat-" 1 4))
          (check (substring>=? "-cat-" 1 4 "-cat-" 1 4)))

(describe substring-ci>=?
          (check (substring-ci>=? "-cat-" 1 5 "-cat-" 1 4))
          (check (substring-ci>=? "-dog-" 1 4 "-cat-" 1 4))
          (check (substring-ci>=? "-DOG-" 1 4 "-cat-" 1 4))
          (check (substring-ci>=? "-cat-" 1 4 "-CAT-" 1 4)))

(define (f-eq) "" "eq")
(define (f-lt) "" "lt")
(define (f-gt) "" "gt")

(describe string-compare
          (check (string-compare "a" "a" f-eq f-lt f-gt) "eq")
          (check (string-compare "a" "b" f-eq f-lt f-gt) "lt")
          (check (string-compare "b" "a" f-eq f-lt f-gt) "gt")
          (check (string-compare "a" "a" (lambda () (+ 1 1)) f-lt f-gt) 2)
          (check (string-compare "a" "b" f-eq (lambda () (+ 1 1)) f-gt) 2)
          (check (string-compare "b" "a" f-eq f-lt (lambda () (+ 1 1))) 2))

(describe string-compare-ci
          (check (string-compare-ci "a" "A" f-eq f-lt f-gt) "eq")
          (check (string-compare-ci "a" "B" f-eq f-lt f-gt) "lt")
          (check (string-compare-ci "b" "A" f-eq f-lt f-gt) "gt")
          (check (string-compare-ci "a" "A" (lambda () (+ 1 1)) f-lt f-gt) 2)
          (check (string-compare-ci "a" "B" f-eq (lambda () (+ 1 1)) f-gt) 2)
          (check (string-compare-ci "b" "A" f-eq f-lt (lambda () (+ 1 1))) 2))

(describe string-hash
          (check (string-hash "hashstring") (string-hash "hashstring"))
          (check! (string-hash "hashstring") (string-hash "hashstrinh")))

(describe string-hash-mod
          (check (string-hash-mod "hashstring" 15) (modulo (string-hash "hashstring") 15)))

(describe string-capitalized?
          (check (string-capitalized? "Hi"))
          (check (string-capitalized? "Hi there"))
          (check (string-capitalized? "Hi There"))
          (check (string-capitalized? "Hi 7465  There"))
          (check! (string-capitalized? "hi")))

(describe substring-capitalized?
          (check (substring-capitalized? "Hi" 0 2))
          (check (substring-capitalized? "Hi there you" 0 8))
          (check (substring-capitalized? "Hi There you" 3 12))
          (check! (substring-capitalized? "Hi there you" 3 12)))

(describe string-lower-case?
          (check (string-lower-case? "hello"))
          (check (string-lower-case? "hello, there 123"))
          (check! (string-lower-case? "Hello")))

(describe substring-lower-case?
          (check! (substring-lower-case? "Hi" 0 2))
          (check! (substring-lower-case? "Hi there you" 0 8))
          (check (substring-lower-case? "Hi there you" 3 12)))

(describe string-upper-case?
          (check! (string-upper-case? "hello"))
          (check (string-upper-case? "HELLO, THERE 123"))
          (check! (string-upper-case? "Hello")))

(describe substring-upper-case?
          (check (substring-upper-case? "HI" 0 2))
          (check! (substring-upper-case? "Hi" 0 2))
          (check (substring-upper-case? "HI THERE you" 0 8))
          (check! (substring-upper-case? "Hi there you" 0 8)))

(describe string-capitalize
          (check (string-capitalize "hello") "Hello")
          (check (string-capitalize "Hello") "Hello")
          (check (string-capitalize "HELLO") "Hello")
          (check (string-capitalize "heLLo") "Hello"))

(describe string-capitalize!
          (define a "heLLo")
          (string-capitalize! a)
          (check a "Hello"))

(describe substring-capitalize!
          (define a "heLLo")
          (substring-capitalize! a 2 4)
          (check a "heLlo"))

(describe string-downcase
          (check (string-downcase "hello") "hello")
          (check (string-downcase "Hello") "hello")
          (check (string-downcase "HELLO") "hello")
          (check (string-downcase "heLLo") "hello"))

(describe string-downcase!
          (define a "heLLo")
          (string-downcase! a)
          (check a "hello"))

(describe substring-downcase!
          (define a "HeLLO")
          (substring-downcase! a 2 4)
          (check a "HellO"))

(describe string-upcase
          (check (string-upcase "hello") "HELLO")
          (check (string-upcase "Hello") "HELLO")
          (check (string-upcase "HELLO") "HELLO")
          (check (string-upcase "heLLo") "HELLO"))

(describe string-upcase!
          (define a "heLLo")
          (string-upcase! a)
          (check a "HELLO"))

(describe substring-upcase!
          (define a "hello")
          (substring-upcase! a 2 4)
          (check a "heLLo"))

(describe string-append
          (check (string-append) "")
          (check (string-append "*" "ace" "*") "*ace*")
          (check (string-append "" "" "") "")
          (define sss "hello")
          (check! (eq? sss (string-append sss))))

(describe substring
          (check (substring "" 0 0) "")
          (check (substring "arduous" 2 5) "duo"))

(describe string-head
          (check (string-head "hello" 3) "hel")
          (check (string-head "hello" 0) ""))

(describe string-head
          (check (string-tail "hello" 3) "lo")
          (check (string-tail "hello" 0) "hello"))

(describe string-pad-left
          (check (string-pad-left "hello" 4) "ello")
          (check (string-pad-left "hello" 8) "   hello")
          (check (string-pad-left "hello" 8 #\*) "***hello"))

(describe string-pad-right
          (check (string-pad-right "hello" 4) "hell")
          (check (string-pad-right "hello" 8) "hello   ")
          (check (string-pad-right "hello" 8 #\*) "hello***"))

(describe string-trim
          (check (string-trim " in the end ") "in the end")
          (check (string-trim " ") "")
          (check (string-trim "100th" "[[:digit:]]") "100"))

(describe string-trim-left
          (check (string-trim-left "-.-+-=-" "\+") "+-=-")
          (check (string-trim-left "   hello") "hello"))

(describe string-trim-right
          (check (string-trim-right "-.-+-=-" "\+") "-.-+")
          (check (string-trim-right "hello   ") "hello"))
