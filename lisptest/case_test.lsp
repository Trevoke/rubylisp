(define (test-func x)
  (case x
    ((0) "zero")
    ((1) "one")
    ((2) "two")
    ((3) "three")
    (else "unknown")))

(describe case
          (check (test-func 0) "zero")
          (check (test-func 1) "one")
          (check (test-func 2) "two")
          (check (test-func 3) "three")
          (check (test-func 5) "unknown"))

;; (define (complex-func x)
;;   (let ((y 1))
;;     (case x
;;       ((0) (set! y 2)
;;          (+ y 1))
;;       ((1) (set! y 5)
;;          (+ y 2))
;;       (else (set! y 10)
;;             (+ y 16)))))

;; (describe complex-case
;;           (check (complex-func 0) 3)
;;           (check (complex-func 1) 7)
;;           (check (complex-func 42) 26))
