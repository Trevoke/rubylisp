describe 'Evaluating' do

  before do
    @env = Lisp::EnvironmentFrame.extending(nil)
  end

  it "a number results in the number" do
    n = Lisp::Number.new(5)
    expect(n.evaluate(@env)).to eq(n)
  end

  it "a string results in the string" do
    s = Lisp::String.new('hello')
    expect(s.evaluate(@env)).to eq(s)
  end

  it "a symbol can be bound to a value" do
    s = Lisp::Symbol.named('test')
    n = Lisp::Number.with_value(5)
    @env.bind(s, n)
    b = @env.binding_for(s)
    expect(b).to_not be_nil
    expect(b.value).to eq(n)
  end

  it "a symbol results in it's bound value" do
    s = Lisp::Symbol.named('test')
    n = Lisp::Number.with_value(5)
    @env.bind(s, n)
    v = s.evaluate(@env)
    expect(v).to_not be_nil
    expect(v).to eq(n)
  end

end
