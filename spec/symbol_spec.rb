describe 'Symbol' do

  it "takes the given value" do
    n = Lisp::Symbol.named('hello')
    expect(n.name).to eq 'hello'
  end

  it "knows what it is" do
    n = Lisp::Symbol.named('hello')
    expect(n.symbol?).to be true
    expect(n.type).to eq :symbol
  end

  it "knows what it isnt" do
    n = Lisp::Symbol.named('hello')
    expect(n.number?).to be false
    expect(n.string?).to be false
    expect(n.pair?).to be false
  end

  it "converts to a string" do
    n = Lisp::Symbol.named("test")
    expect(n.to_s).to eq 'test'
  end

end
