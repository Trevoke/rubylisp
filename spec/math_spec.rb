describe "Math" do

  before do
    @p = Lisp::Parser.new
  end

  it "adds 1 number (returns that number)" do
    sexpr, eof = @p.parse("(+ 1)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 1
  end

  it "adds two numbers" do
    sexpr, eof = @p.parse("(+ 1 2)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 3
  end

  it "adds many numbers" do
    sexpr, eof = @p.parse("(+ 1 2 3 4 5 6)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 21
  end

  it "subtracts 1 number (returns the negative of that number)" do
    sexpr, eof = @p.parse("(- 1)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq -1
  end

  it "subtracts two numbers" do
    sexpr, eof = @p.parse("(- 2 1)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 1
  end

  it "subtracts two numbers resulting in a negative" do
    sexpr, eof = @p.parse("(- 1 2)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq -1
  end

  it "subtracts many numbers" do
    sexpr, eof = @p.parse("(- 10 2 3 3)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 2
  end

  it "multiplies 1 number (returns that number)" do
    sexpr, eof = @p.parse("(* 5)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 5
  end

  it "multiplies two numbers" do
    sexpr, eof = @p.parse("(* 5 2)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 10
  end

  it "multiplies many numbers" do
    sexpr, eof = @p.parse("(* 5 2 3 4)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 120
  end

  it "divides 1 number (returns that number)" do
    sexpr, eof = @p.parse("(/ 5)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 5
  end

  it "divides two numbers" do
    sexpr, eof = @p.parse("(/ 15 3)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 5
  end

  it "divides many numbers" do
    sexpr, eof = @p.parse("(/ 120 6 5 2)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 2
  end

  it "mods 1 number (returns that number)" do
    sexpr, eof = @p.parse("(% 5)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 5
  end

  it "mods two numbers" do
    sexpr, eof = @p.parse("(% 5 2)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 1
  end

  it "mods many numbers" do
    sexpr, eof = @p.parse("(% 110 7 3)")
    val = sexpr.evaluate(Lisp::EnvironmentFrame.global)
    expect(val.type).to eq :number
    expect(val.value).to eq 2
  end

end
