describe 'cons cell' do

  it "has two nil pointers when newly created" do
    c = Lisp::ConsCell.new()
    expect(c.car).to be_nil
    expect(c.cdr).to be_nil
  end

  it "captures the pointers given to it" do
    a = Lisp::Number.with_value(1)
    b = Lisp::Number.with_value(2)
    c = Lisp::ConsCell.cons(a, b)
    expect(c.car).to eql a
    expect(c.cdr).to eql b
  end

  it "prints as an empty list" do
    c = Lisp::ConsCell.new
    expect(c.to_s).to eq "()"
  end

  it "prints as a dotted pair" do
    a = Lisp::Number.with_value(1)
    b = Lisp::Number.with_value(2)
    c = Lisp::ConsCell.cons(a, b)
    expect(c.to_s).to eq "(1 . 2)"
  end

  it "prints as a single element list" do
    a = Lisp::Number.with_value(1)
    c = Lisp::ConsCell.cons(a, nil)
    expect(c.to_s).to eq "(1)"
  end

  it "prints as a multi-element list" do
    a = Lisp::Number.with_value(1)
    b = Lisp::Number.with_value(2)
    c = Lisp::Number.with_value(3)
    d = Lisp::Number.with_value(4)
    l = Lisp::ConsCell.cons(a, Lisp::ConsCell.cons(b, Lisp::ConsCell.cons(c, Lisp::ConsCell.cons(d, nil))))
    expect(l.to_s).to eq "(1 2 3 4)"
  end

  it "supports all?" do
    list = Lisp::Parser.new.parse("(1 2 3 4)")
    expect(list.all? {|e| e.type == :number}).to be true
  end

  it "can convert into an array" do
    list = Lisp::Parser.new.parse("(1 2 3 4)")
    a = list.to_a
    expect(a.length).to eq 4
    expect(a[0].value).to eq 1
    expect(a[1].value).to eq 2
    expect(a[2].value).to eq 3
    expect(a[3].value).to eq 4
  end

  it "can evaluate each sexpr" do
    body = Lisp::Parser.new.parse("((+ 1 1) (+ 2 2) (+ 3 3))")
    expect(body.evaluate_each(Lisp::EnvironmentFrame.global).value).to eq 6
  end
end
