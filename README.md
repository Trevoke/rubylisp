## Overview

RubyLisp is a relatively simple Lisp dialect based on Scheme
implimented using Ruby.

RubyLisp is a lexically scoped Lisp-1:

**Lexically scoped:**  
The scope of definitions are determined by where they are made in the
lexical structure of the code. Each `lambda`, `let`, and `do` creates
a new lexical scope. From [2]:

> Here references to the established entity can occur only within
> certain program portions that are lexically (that is, textually)
> contained within the establishing construct. Typically the construct
> will have a part designated the body, and the scope of all entities
> established will be (or include) the body.

**Lisp-1:**  
A Lisp where functions and variables share a single namespace. This
differs from a Lisp-2 in which functions and variables have separate
namespaces.

## Installation

Add this line to your application's Gemfile:

    gem 'rubylisp'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rubylisp

## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request

## REPL

RubyLisp includes a very basic REPL that, as expected, lets you type
in snippets (it does not support multiline snippets) of Lisp code,
evaluates them, and prints the result.

    >: rubylisp

    RubyLisp REPL
    > 4
    4
    > (+ 2 3)
    5
    > (define (fib x) (if (eq? x 1) 1 (+ x (fib (- x 1)))))
    <function: fib>
    > (fib 4)
    24

## Integrating


The simplist way to integrate RubyLisp is to have it parse and
evaluate strings of lisp code.

    >: irb
    2.0.0-p247 :001 > require 'rubylisp'
     => true
    2.0.0-p247 :002 > Lisp::Initializer.register_builtins
     => ...
    2.0.0-p247 :003 > Lisp::Parser.new.parse_and_eval('(+ 1 2)').to_s
     => "3"
    2.0.0-p247 :004 >

But lets say you are using rubylisp to do something more embedded such as provide an extension language to your application. It's easy to provide the Lisp runtime with functions written in Ruby that can be called from Lisp code.

to be continued...

