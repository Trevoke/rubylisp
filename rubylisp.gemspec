Gem::Specification.new do |gem|
  gem.name        = 'rubylisp'
  gem.version     = '0.2.0'
  gem.bindir      = 'bin'
  gem.executables << 'rubylisp'
  gem.date        = '2014-11-16'
  gem.summary     = "Lisp in Ruby"
  gem.description = "An embeddable Lisp as an extension language for Ruby"
  gem.authors     = ["Dave Astels"]
  gem.email       = 'dastels@icloud.com'
  gem.files       = ["lib/rubylisp.rb",
                     "lib/rubylisp/alist.rb",
                     "lib/rubylisp/assignment.rb",
                     "lib/rubylisp/atom.rb",
                     "lib/rubylisp/binding.rb",
                     "lib/rubylisp/boolean.rb",
                     "lib/rubylisp/builtins.rb",
                     "lib/rubylisp/character.rb",
                     "lib/rubylisp/cons_cell.rb",
                     "lib/rubylisp/debug.rb",
                     "lib/rubylisp/environment_frame.rb",
                     "lib/rubylisp/equivalence.rb",
                     "lib/rubylisp/exception.rb",
                     "lib/rubylisp/ext.rb",
                     "lib/rubylisp/ffi_class.rb",
                     "lib/rubylisp/ffi_new.rb",
                     "lib/rubylisp/ffi_send.rb",
                     "lib/rubylisp/ffi_static.rb",
                     "lib/rubylisp/frame.rb",
                     "lib/rubylisp/function.rb",
                     "lib/rubylisp/io.rb",
                     "lib/rubylisp/list_support.rb",
                     "lib/rubylisp/logical.rb",
                     "lib/rubylisp/macro.rb",
                     "lib/rubylisp/math.rb",
                     "lib/rubylisp/number.rb",
                     "lib/rubylisp/object.rb",
                     "lib/rubylisp/parser.rb",
                     "lib/rubylisp/primitive.rb",
                     "lib/rubylisp/relational.rb",
                     "lib/rubylisp/special_forms.rb",
                     "lib/rubylisp/string.rb",
                     "lib/rubylisp/symbol.rb",
                     "lib/rubylisp/system.rb",
                     "lib/rubylisp/testing.rb",
                     "lib/rubylisp/tokenizer.rb",
                     "lib/rubylisp/type_checks.rb",
                     "lib/rubylisp/vector.rb",
                     "README.md"]
  gem.homepage    = 'https://bitbucket.org/dastels/rubylisp'
  gem.license     = 'MIT'
end
