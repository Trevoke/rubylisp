module Lisp

  class ListSupport

    def self.register
      %w(car cdr caar cadr cdar cddr
         caaar caadr cadar caddr cdaar cdadr cddar cdddr
         caaaar caaadr caadar caaddr cadaar cadadr caddar cadddr
         cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr).each do |name|
        Primitive.register(name) {|args, env| ad_impl(args, env, name) }
      end
      Primitive.register("list") {|args, env|  Lisp::ListSupport::list_impl(args, env) }
      Primitive.register("vector") {|args, env|  Lisp::ListSupport::vector_impl(args, env) }
      Primitive.register("cons*") {|args, env|  Lisp::ListSupport::cons_star_impl(args, env) }
      Primitive.register("cons") {|args, env|  Lisp::ListSupport::cons_impl(args, env) }
      Primitive.register("make-list") {|args, env|  Lisp::ListSupport::make_list_impl(args, env) }
      Primitive.register("iota") {|args, env|  Lisp::ListSupport::iota_impl(args, env) }

      Primitive.register("length") {|args, env|  Lisp::ListSupport::length_impl(args, env) }
      Primitive.register("first") {|args, env|  Lisp::ListSupport::first_impl(args, env) }
      Primitive.register("head") {|args, env|  Lisp::ListSupport::first_impl(args, env) }
      Primitive.register("rest") {|args, env|  Lisp::ListSupport::rest_impl(args, env) }
      Primitive.register("tail") {|args, env|  Lisp::ListSupport::rest_impl(args, env) }
      Primitive.register("second") {|args, env|  Lisp::ListSupport::second_impl(args, env) }
      Primitive.register("third") {|args, env|  Lisp::ListSupport::third_impl(args, env) }
      Primitive.register("fourth") {|args, env|  Lisp::ListSupport::fourth_impl(args, env) }
      Primitive.register("fifth") {|args, env|  Lisp::ListSupport::fifth_impl(args, env) }
      Primitive.register("sixth") {|args, env|  Lisp::ListSupport::sixth_impl(args, env) }
      Primitive.register("seventh") {|args, env|  Lisp::ListSupport::seventh_impl(args, env) }
      Primitive.register("eighth") {|args, env|  Lisp::ListSupport::eighth_impl(args, env) }
      Primitive.register("ninth") {|args, env|  Lisp::ListSupport::ninth_impl(args, env) }
      Primitive.register("tenth") {|args, env|  Lisp::ListSupport::tenth_impl(args, env) }
      Primitive.register("nth") {|args, env|  Lisp::ListSupport::nth_impl(args, env) }

      Primitive.register("sublist") {|args, env|  Lisp::ListSupport::sublist_impl(args, env) }
      Primitive.register("list-head") {|args, env|  Lisp::ListSupport::list_head_impl(args, env) }
      Primitive.register("take") {|args, env|  Lisp::ListSupport::take_impl(args, env) }
      Primitive.register("list-tail") {|args, env|  Lisp::ListSupport::list_tail_impl(args, env) }
      Primitive.register("drop") {|args, env|  Lisp::ListSupport::drop_impl(args, env) }
      Primitive.register("last-pair") {|args, env|  Lisp::ListSupport::last_pair_impl(args, env) }

      Primitive.register("memq") {|args, env|  Lisp::ListSupport::memq_impl(args, env) }
      Primitive.register("memv") {|args, env|  Lisp::ListSupport::memv_impl(args, env) }
      Primitive.register("member") {|args, env|  Lisp::ListSupport::member_impl(args, env) }

      Primitive.register("filter")         {|args, env| Lisp::ListSupport::filter_impl(args, env) }
      Primitive.register("remove")         {|args, env| Lisp::ListSupport::remove_impl(args, env) }
      Primitive.register("partition")         {|args, env| Lisp::ListSupport::partition_impl(args, env) }
      Primitive.register("map")         {|args, env| Lisp::ListSupport::map_impl(args, env) }
      Primitive.register("reduce-left") {|args, env| Lisp::ListSupport::reduce_left_impl(args, env) }
      Primitive.register("any") {|args, env| Lisp::ListSupport::any_impl(args, env) }
      Primitive.register("every") {|args, env| Lisp::ListSupport::every_impl(args, env) }
      Primitive.register("reverse")     {|args, env| Lisp::ListSupport::reverse_impl(args, env) }
      Primitive.register("append")      {|args, env| Lisp::ListSupport::append_impl(args, env) }
      Primitive.register("append!")     {|args, env| Lisp::ListSupport::appendbang_impl(args, env) }
      Primitive.register("flatten")     {|args, env| Lisp::ListSupport::flatten_impl(args, env) }
      # Primitive.register("flatten*")     {|args, env| Lisp::ListSupport::recursive_flatten_impl(args, env) }
    end


    def self.cons_impl(args, env)
      return Lisp::Debug.process_error("cons requires two arguments.", env) unless args.length == 2
      left = args.car.evaluate(env)
      right = args.cadr.evaluate(env)
      Lisp::ConsCell.cons(left, right)
    end


    def self.cons_star_impl(args, env)
      vals = []
      args.each {|item| vals << item.evaluate(env) }
      Lisp::ConsCell::array_to_list(vals[0..-2], vals[-1])
    end


    def self.list_impl(args, env)
      vals = []
      args.each {|item| vals << item.evaluate(env) }
      if vals.size == 1 && vals[0].vector?
        Lisp::ConsCell::array_to_list(vals[0].value)
      else
        Lisp::ConsCell::array_to_list(vals)
      end
    end


    def self.make_list_impl(args, env)
      return Lisp::Debug.process_error("consmake-list requires one or two arguments.", env) unless args.length == 1 || args.length == 2
      arg1 = args.car.evaluate(env)
      return Lisp::Debug.process_error("make-list requires an integer for it's first argument, received: #{args.car}", env) unless arg1.integer?
      count = arg1.value
      val = if args.length == 1
              nil
            else
              args.cadr.evaluate(env)
            end

      vals = Array.new(count, val)
      Lisp::ConsCell::array_to_list(vals)
    end


    def self.iota_impl(args, env)
      return Lisp::Debug.process_error("iota requires at least one argument.", env) unless args.length > 0
      arg1 = args.car.evaluate(env)
      return Lisp::Debug.process_error("iota requires an positive integer for it's first argument, received: #{args.car}", env) unless arg1.integer? && arg1.positive?
      count = arg1.value

      start = if args.length < 2
                0
              else
                arg2 = args.cadr.evaluate(env)
                return Lisp::Debug.process_error("iota requires an number for it's second argument, received: #{args.cadr}", env) unless arg2.number?
                arg2.value
              end

      step = if args.length < 3
                1
              else
                arg3 = args.caddr.evaluate(env)
                return Lisp::Debug.process_error("iota requires an number for it's third argument, received: #{args.caddr}", env) unless arg3.number?
                arg3.value
              end

      vals = []
      count.times do |c|
        vals << start
        start += step
      end

      Lisp::ConsCell::array_to_list(vals.map {|v| Number.with_value(v) })
    end


    def self.length_impl(args, env)
      Lisp::Number.with_value(args.car.evaluate(env).length)
    end


    # in support of all the CxR functions
    def self.ad_impl(args, env, f)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("list required.", env) unless l.list?
      l.send(f)
    end


    def self.first_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 1
      l.nth(1)
    end


    def self.rest_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      if l.list?
        l.cdr
      else
        Lisp::Vector.new(l.value[1..-1])
      end
    end


    def self.second_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 2
      l.nth(2)
    end

    def self.third_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 3
      l.nth(3)
    end


    def self.fourth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 4
      l.nth(4)
    end


    def self.fifth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 5
      l.nth(5)
    end


    def self.sixth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 6
      l.nth(6)
    end


    def self.seventh_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 7
      l.nth(7)
    end


    def self.eighth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 8
      l.nth(8)
    end


    def self.ninth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 9
      l.nth(9)
    end


    def self.tenth_impl(args, env)
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("list index out of bounds", env) unless l.length >= 10
      l.nth(10)
    end


    def self.nth_impl(args, env)
      return Lisp::Debug.process_error("nth requires 2 arguments", env) unless args.length == 2
      n = args.car.evaluate(env)
      return Lisp::Debug.process_error("The first argument of nth has to be an number.", env) unless n.number?
      return Lisp::Debug.process_error("The first argument of nth has to be positive.", env) unless n.value > 0
      l = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("rest requires a list or vector.", env) unless l.list? || l.vector?
      l.nth(n.value)
    end


    def self.make_same_kind_as(sequence, value)
      if sequence.vector?
        Lisp::Vector.new(value)
      else
        Lisp::ConsCell.array_to_list(value)
      end
    end
    

    def self.sublist_impl(args, env)
      return Lisp::Debug.process_error("sublist requires 3 arguments", env) unless args.length == 3
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("sublist requires it's first argument to be a list or vector, but received #{args.car}", env) unless l.list? || l.vector?
      st = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("sublist requires it's second argument to be a positive integer, but received #{args.cadr}", env) unless st.number? && st.positive?
      return Lisp::Debug.process_error("sublist requires it's second argument to be <= the list length", env) unless st.value <= l.length
      en = args.caddr.evaluate(env)
      return Lisp::Debug.process_error("sublist requires it's third argument to be a positive integer, but received #{args.caddr}", env) unless en.number? && en.positive?
      return Lisp::Debug.process_error("sublist requires it's third argument to be <= the list length", env) unless en.value <= l.length
      return Lisp::Debug.process_error("sublist requires it's second argument to be <= the third argument", env) unless st.value <= en.value
      make_same_kind_as(l, l.to_a[(st.value - 1)...en.value])
    end


    def self.list_head_impl(args, env)
      return Lisp::Debug.process_error("list_head requires 2 arguments", env) unless args.length == 2
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("list_head requires it's first argument to be a list, but received #{args.car}", env) unless l.list?
      k = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("list_head requires it's second argument to be a positive integer, but received #{args.cadr}", env) unless k.number? && k.positive?
      return Lisp::Debug.process_error("list_head requires it's second argument to be <= the list length", env) unless k.value <= l.length
      Lisp::ConsCell.array_to_list(l.to_a[0...k.value])
    end


    def self.take_impl(args, env)
      return Lisp::Debug.process_error("take requires 2 arguments", env) unless args.length == 2
      k = args.car.evaluate(env)
      return Lisp::Debug.process_error("take requires it's first argument to be an integer >= 0, but received #{args.car}", env) unless k.number? && !k.negative?
      l = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("take requires it's second argument to be a list or vector, but received #{args.cadr}", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("take requires it's first argument to be <= the list length", env) unless k.value <= l.length
      make_same_kind_as(l, l.to_a[0...k.value])
    end


    def self.list_tail_impl(args, env)
      return Lisp::Debug.process_error("list_head requires 2 arguments", env) unless args.length == 2
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("list_head requires it's first argument to be a list, but received #{args.car}", env) unless l.list?
      k = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("list_head requires it's second argument to be a positive integer, but received #{args.cadr}", env) unless k.number? && k.positive?
      return Lisp::Debug.process_error("list_head requires it's second argument to be <= the list length", env) unless k.value <= l.length
      l.nth_tail(k.value + 1)
    end


    def self.drop_impl(args, env)
      return Lisp::Debug.process_error("drop requires 2 arguments", env) unless args.length == 2
      k = args.car.evaluate(env)
      return Lisp::Debug.process_error("drop requires it's first argument to be an integer >= 0, but received #{args.car}", env) unless k.number? && !k.negative?
      l = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("drop requires it's second argument to be a list or vector, but received #{args.cadr}", env) unless l.list? || l.vector?
      return Lisp::Debug.process_error("drop requires it's first argument to be <= the list length", env) unless k.value <= l.length
      l.nth_tail(k.value + 1)
    end


    def self.last_pair_impl(args, env)
      return Lisp::Debug.process_error("last_pair requires 1 arguments", env) unless args.length == 1
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("last_pair requires it's argument to be a list, but received #{args.car}", env) unless l.list?
      l.last
    end


    def self.memq_impl(args, env)
      return Lisp::Debug.process_error("memq requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      item = args.car.evaluate(env)
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("memq requires a list as it's second argument.", env) unless collection.list?
      collection.length.times do |i|
        if Lisp::Equivalence.eq_check(item, collection.nth(i + 1)).value
          return collection.nth_tail(i + 1)
        end
      end
      Lisp::FALSE
    end


    def self.memv_impl(args, env)
      return Lisp::Debug.process_error("memv requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      item = args.car.evaluate(env)
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("memv requires a list as it's second argument.", env) unless collection.list?
      collection.length.times do |i|
        if Lisp::Equivalence.eqv_check(item, collection.nth(i + 1)).value
          return collection.nth_tail(i + 1)
        end
      end
      Lisp::FALSE
    end


    def self.member_impl(args, env)
      return Lisp::Debug.process_error("member requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      item = args.car.evaluate(env)
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("member requires a list as it's second argument.", env) unless collection.list?
      collection.length.times do |i|
        if Lisp::Equivalence.equal_check(item, collection.nth(i + 1)).value
          return collection.nth_tail(i + 1)
        end
      end
      Lisp::FALSE
    end


    def self.filter_impl(args, env)
      return Lisp::Debug.process_error("filter requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      f = args.car.evaluate(env)
      return Lisp::Debug.process_error("filter requires a function as it's first argument but received #{args.car}.", env) unless f.function? || f.primitive?
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("filter requires a list or vector as it's second argument but received #{args.cadr}.", env) unless collection.list? || collection.vector?
      results = collection.to_a.select {|item| f.apply_to_without_evaluating(Lisp::ConsCell.cons(item, nil), env).value }
      make_same_kind_as(collection, results)
    end


    def self.remove_impl(args, env)
      return Lisp::Debug.process_error("remove requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      f = args.car.evaluate(env)
      return Lisp::Debug.process_error("remove requires a function as it's first argument but received #{args.car}.", env) unless f.function? || f.primitive?
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("remove requires a list or vector as it's second argument but received #{args.cadr}.", env) unless collection.list? || collection.vector?
      results = collection.to_a.reject {|item| f.apply_to_without_evaluating(Lisp::ConsCell.cons(item, nil), env).value }
      make_same_kind_as(collection, results)
    end


    def self.partition_impl(args, env)
      return Lisp::Debug.process_error("partition requires 2 arguments but received #{args.length}.", env) unless args.length == 2
      f = args.car.evaluate(env)
      return Lisp::Debug.process_error("partition requires a function as it's first argument.", env) unless f.function? || f.primitive?
      collection = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("partition requires a list as it's second argument.", env) unless collection.list? | collection.vector?
      results = collection.to_a.partition {|item| f.apply_to_without_evaluating(Lisp::ConsCell.cons(item, nil), env).value }
      matches = make_same_kind_as(collection, results[0])
      non_matches = make_same_kind_as(collection, results[1])
      Lisp::ConsCell.array_to_list([matches, non_matches])
    end


    def self.map_impl(args, env)
      return Lisp::Debug.process_error("map requires at least 2 arguments but received #{args.length}.", env) if args.length < 2
      f = args.car.evaluate(env)
      return Lisp::Debug.process_error("map requires a function as it's first argument but received #{args.car}.", env) unless f.function? || f.primitive?
      collections = args.cdr.to_a.collect {|a| a.evaluate(env)}
      return Lisp::Debug.process_error("all requires all subsequent arguments to be lists or vectors", env) unless collections.all? {|l| l.list? || l.vector?}
      all_vectors = collections.all? {|i| i.vector?}
      lists = collections.collect {|l| l.to_a }
      
      map_args = []
      while (lists.all? {|l| !l.empty? })
        map_args << Lisp::ConsCell.array_to_list(lists.map {|l| l.shift })
      end
      results = map_args.collect {|item| f.apply_to_without_evaluating(item, env) }
      if all_vectors
        Lisp::Vector.new(results)
      else
        Lisp::ConsCell.array_to_list(results)
      end

    end


    def self.quote_if_required(thing)
      return thing unless thing.list? || thing.symbol?
      thing.quoted
    end


    def self.reduce_left_impl(args, env)
      return Lisp::Debug.process_error("reduce requires 3 arguments but received #{args.length}.", env) unless args.length == 3
      f = args.car.evaluate(env)
      return Lisp::Debug.process_error("map requires a function as it's first argument but received #{args.car}.", env) unless f.function? || f.primitive?
      initial = args.cadr.evaluate(env)
      collection = args.caddr.evaluate(env)
      return Lisp::Debug.process_error("reduce requires a list or vector as it's third argument but received #{args.caddr}.", env) unless collection.list? || collection.vector?
      return initial if collection.empty?
      return collection.nth(1) if collection.length == 1
      result = collection.to_a.inject do |acc, item|
        f.apply_to(Lisp::ConsCell.array_to_list([quote_if_required(acc), quote_if_required(item)]), env)
      end
      result
    end


    def self.any_impl(args, env)
      return Lisp::Debug.process_error("any requires at least two arguments", env) unless args.length >= 2
      p = args.car.evaluate(env)
      return Lisp::Debug.process_error("any requires a function as it's first argument", env) unless p.function? || p.primitive?
      lists = args.cdr.to_a.collect {|a| a.evaluate(env)}
      return Lisp::Debug.process_error("any requires all subsequent arguments to be lists or vectors", env) unless lists.all? {|l| l.list? || l.vector?}

      while true
        cars = lists.collect {|l| l.nth(1)}
        return_val = p.apply_to(Lisp::ConsCell.array_to_list(cars), env)
        return Lisp::TRUE if return_val.value
        lists = lists.collect {|l| l.nth_tail(2)}
        return Lisp::FALSE if lists.any? {|l| l.empty?}
      end
    end


    def self.every_impl(args, env)
      return Lisp::Debug.process_error("all requires at least two arguments", env) unless args.length >= 2
      p = args.car.evaluate(env)
      return Lisp::Debug.process_error("all requires a function as it's first argument", env) unless p.function? || p.primitive?
      lists = args.cdr.to_a.collect {|a| a.evaluate(env)}
      return Lisp::Debug.process_error("all requires all subsequent arguments to be lists or vectors", env) unless lists.all? {|l| l.list? || l.vector?}

      while true
        cars = lists.collect {|l| l.nth(1)}
        return_val = p.apply_to(Lisp::ConsCell.array_to_list(cars), env)
        return Lisp::FALSE unless return_val.value
        lists = lists.collect {|l| l.nth_tail(2)}
        return Lisp::TRUE if lists.any? {|l| l.empty?}
      end
    end


    def self.reverse_impl(args, env)
      return Lisp::Debug.process_error("reverse requires a single argument.", env) unless args.length == 1
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("reverse requires a list or vector", env) unless l.list? || l.vector?
      make_same_kind_as(l, l.to_a.reverse)
    end


    def self.append_impl(args, env)
      return Lisp::Debug.process_error("append requires at least 1 argument.", env) unless args.length >= 1
      l = args.map {|i| i.evaluate(env)}
      return Lisp::Debug.process_error("append requires lists or vectors", env) unless l.all? {|i| i.list? || i.vector?}
      all_vectors = l.all? {|i| i.vector?}
      new_items = []
      l.each do |sublist|
        sublist.each {|item| new_items << item.copy}
      end

      if all_vectors
        Lisp::Vector.new(new_items)
      else
        Lisp::ConsCell.array_to_list(new_items)
      end
    end


    def self.appendbang_impl(args, env)
      return Lisp::Debug.process_error("append! requires at least 1 argument.", env) unless args.length >= 1
      arg_array = args.to_a.map {|i| i.evaluate(env)}
      return Lisp::Debug.process_error("append! requires lists", env) unless arg_array.all? {|i| i.list?}
      (0...(arg_array.length-1)). each do |i|
        arg_array[i].last.set_cdr!(arg_array[i+1])
      end
      arg_array[0]
    end


    def self.flatten_impl(args, env)
      return Lisp::Debug.process_error("flatten requires 1 argument.", env) unless args.length != 1
      l = args.car.evaluate(env)
      return Lisp::Debug.process_error("flatten requires a list argument", env) unless l.list?
      l.flatten
    end

  end
end
