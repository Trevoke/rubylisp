module Lisp

  class Tokenizer

    attr_reader :line_number

    def initialize(src)
      @lookahead_token = 0
      @lookahead_literal = ''
      @source = src
      @position = 0
    end

    def next_token
      return @lookahead_token, @lookahead_literal
    end

    def eof?
      @position >= @source.length
    end

    def almost_eof?
      @position == @source.length - 1
    end

    def next_char
      almost_eof? ? nil : @source[@position + 1]
    end

    def letter?(ch)
      ch =~ /[[:alpha:]]/
    end

    def hex?(ch)
      ch =~ /[abcdefABCDEF]/
    end

    def digit?(ch)
      ch =~ /[[:digit:]]/
    end

    def number?(ch)
      digit?(ch) || (ch == '-' && digit?(next_char))
    end


    def space?(ch)
      ch =~ /[[:space:]]/
    end


    def symbol_character?(ch)
      return true if letter?(ch)
      return true if (?0..?9).include?(ch)
      return "-_?!:*=<>".include?(ch)
    end

    def read_symbol
      start = @position
      tok = nil
      if @source[@position] == '.'
        @position += 1
        tok = :FFI_SEND_SYMBOL
      end

      while !eof? && (symbol_character?(@source[@position]) ||
                      (@source[@position] == '.' && !symbol_character?(@source[@position+1]) && tok.nil?) ||
                      (@source[@position] == '/' && symbol_character?(@source[@position+1])))
        tok ||= :FFI_NEW_SYMBOL if @source[@position] == '.'
        tok = :FFI_STATIC_SYMBOL if @source[@position] == '/'
        @position += 1
      end

      tok ||= :SYMBOL
      return tok, case tok
                  when :SYMBOL, :FFI_STATIC_SYMBOL
                    @source[start...@position]
                  when :FFI_SEND_SYMBOL
                    @source[start+1...@position]
                  when :FFI_NEW_SYMBOL
                    @source[start...@position-1]
                  end
    end

    def read_number
      start = @position
      @position += 1 if @source[@position] == '-'
      hex = @source[@position, 2] == "#x"
      is_float = false
      @position += 2 if hex
      ch = @source[@position]
      while !eof? && (digit?(ch) || (hex && hex?(ch)) || (!hex && !is_float && ch == ?.))
        is_float ||= (ch == ?.)
        @position += 1
        ch = @source[@position]
      end

      tok = if hex
              :HEXNUMBER
            elsif is_float
              :FLOAT
            else
              :NUMBER
            end

      return tok, @source[start...@position]
    end


    def process_escapes(str)
      i = 0
      processed_str = ""
      while i < str.length
        if str[i] == ?\\
          processed_str << if i < (str.length - 1)
                             i += 1
                             case (str[i])
                             when ?n
                               "\n"
                             when ?t
                               "\t"
                             when ?\\
                               "\\"
                             else
                               "\\#{str[i]}"
                             end
                           else
                             "\\"
                           end
        else
          processed_str << str[i]
        end
        i += 1
      end
      processed_str
    end


    def read_string
      start = @position
      @position += 1
      while !eof? && @source[@position] != ?"
        @position += 1
      end

      return :EOF, '' if eof?
      @position += 1
      return :STRING, process_escapes(@source[start...@position])
    end


    def divider?(ch)
      ch =~ /[[[:space:]]\(\)\{\}<>\[\]]/
    end


    def read_character
      @position += 2
      start = @position
      @position += 1
      while !eof? && !divider?(@source[@position])
        @position += 1
      end

      return :CHARACTER, @source[start...@position]
    end


    def read_next_token
      return :EOF, '' if eof?

      while space?(@source[@position])
        @line_number += 1 if @source[@position] == ?\n
        @position += 1
        return :EOF, '' if eof?
      end

      current_ch = @source[@position]
      next_ch = @source[@position + 1] unless almost_eof?

      if letter?(current_ch) || ('*._'.include?(current_ch) && letter?(next_ch))
        return read_symbol
      elsif number?(current_ch)
        return read_number
      elsif current_ch == ?- && number?(next_ch)
        return read_number
      elsif current_ch == ?# && next_ch == ?x
        return read_number
      elsif current_ch == ?"
        return read_string
      elsif current_ch == ?# && next_ch == ?\\
        return read_character
      elsif current_ch == ?'
        @position += 1
        return :QUOTE, "'"
      elsif current_ch == ?`
        @position += 1
        return :BACKQUOTE, "`"
      elsif current_ch == ?, && next_ch == ?@
        @position += 2
        return :COMMAAT, ",@"
      elsif current_ch == ?,
        @position += 1
        return :COMMA, ","
      elsif current_ch == ?(
        @position += 1
        return :LPAREN, "("
      elsif current_ch == ?)
        @position += 1
        return :RPAREN, ")"
      elsif current_ch == ?{
        @position += 1
        return :LBRACE, "{"
      elsif current_ch == ?}
        @position += 1
        return :RBRACE, "}"
      elsif current_ch == ?[
        @position += 1
        return :LBRACKET, "["
      elsif current_ch == ?]
        @position += 1
        return :RBRACKET, "]"
      elsif current_ch == ?.
        @position += 1
        return :PERIOD, "."
      elsif current_ch == ?/ && next_ch == ?=
        @position += 2
        return :SYMBOL, "!="
      elsif current_ch == ?- && next_ch == ?>
        @position += 2
        return :SYMBOL, "->"
      elsif current_ch == ?= && next_ch == ?>
        @position += 2
        return :SYMBOL, "=>"
      elsif "+-*/%".include?(current_ch)
        @position += 1
        return :SYMBOL, current_ch.to_s
      elsif current_ch == ?< && next_ch == ?=
        @position += 2
        return :SYMBOL, "<="
      elsif current_ch == ?<
        @position += 1
        return :SYMBOL, "<"
      elsif current_ch == ?> && next_ch == ?=
        @position += 2
        return :SYMBOL, ">="
      elsif current_ch == ?>
        @position += 1
        return :SYMBOL, ">"
      elsif current_ch == ?= && next_ch == ?=
        @position += 2
        return :SYMBOL, "="
      elsif current_ch == ?=
        @position += 1
        return :SYMBOL, "="
      elsif current_ch == ?! && next_ch == ?=
        @position += 2
        return :SYMBOL, "!="
      elsif current_ch == ?!
        @position += 1
        return :SYMBOL, "!"
      elsif current_ch == ?# && next_ch == ?t
        @position += 2
        return :TRUE, "#t"
      elsif current_ch == ?# && next_ch == ?f
        @position += 2
        return :FALSE, "#f"
      elsif current_ch == ?;
        start = @position
        while true
          return :COMMENT, @source[start..-1] if eof?
          return :COMMENT, @source[start...@position] if @source[@position] == ?\n
          @position += 1
        end
      else
        return :ILLEGAL, ''
      end
    end

    def consume_token
      @lookahead_token, @lookahead_literal = read_next_token
      consume_token if @lookahead_token == :COMMENT
    end

    def init
      @line_number = 0
      consume_token
    end

  end

end
