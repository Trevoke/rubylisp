module Lisp

  class Relational

    def self.register
      Primitive.register("<", "(< number number)\n\nReturns whether the first argument is less than the second argument.") do |args, env|
        Lisp::Relational::lt_impl(args, env)
      end

      Primitive.register(">", "(> number number)\n\nReturns whether the first argument is greater than the second argument.") do |args, env|
        Lisp::Relational::gt_impl(args, env)
      end

      Primitive.register("<=", "(<= number number)\n\nReturns whether the first argument is less than or equal to the second argument.") do |args, env|
        Lisp::Relational::lteq_impl(args, env)
      end

      Primitive.register(">=", "(>= number number)\n\nReturns whether the first argument is greater than or equal to the second argument.") do |args, env|
        Lisp::Relational::gteq_impl(args, env)
      end
    end


    def self.lt_impl(args, env)
      return Lisp::Debug.process_error("< needs at least 2 arguments", env) unless args.length > 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).value < args.cadr.evaluate(env).value)
    end

    def self.gt_impl(args, env)
      return Lisp::Debug.process_error("> needs at least 2 arguments", env) unless args.length > 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).value > args.cadr.evaluate(env).value)
    end

    def self.lteq_impl(args, env)
      return Lisp::Debug.process_error("<= needs at least 2 arguments", env) unless args.length > 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).value <= args.cadr.evaluate(env).value)
    end

    def self.gteq_impl(args, env)
      return Lisp::Debug.process_error(">= needs at least 2 arguments", env) unless args.length > 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).value >= args.cadr.evaluate(env).value)
    end


  end
end
