module Lisp

  class System

    def self.register
      Primitive.register("sleep")         {|args, env| Lisp::System.sleep_impl(args, env) }
      Primitive.register("quit")          {|args, env| exit() }
    end


    def self.sleep_impl(args, env)
      return Lisp::Debug.process_error("sleep needs 1 argument", env) if args.length != 1
      arg = args.car.evaluate(env)
      return Lisp::Debug.process_error("sleep needs a numeric argument", env) unless arg.number?
      sleep(arg.value)
    end

    
  end
end
