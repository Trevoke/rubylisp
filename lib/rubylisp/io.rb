module Lisp

  class IO

    def self.register
      Primitive.register("load")         {|args, env| Lisp::IO::load_impl(args, env) }
      Primitive.register("load-library") {|args, env| Lisp::IO::load_library_impl(args, env) }
      Primitive.register("load-project") {|args, env| Lisp::IO::load_project_impl(args, env) }
      Primitive.register("trace")        {|args, env| puts "Trace: #{(args.to_a.map {|a| a.evaluate(env).value.to_s}).join(' ')}"; nil} 
      Primitive.register("error")        {|args, env| App.alert((args.to_a.map {|a| a.evaluate(env).value.to_s}).join(' '))}
      Primitive.register("alert")        {|args, env| App.alert((args.to_a.map {|a| a.evaluate(env).value.to_s}).join(' '))}
   end


    def self.load_impl(args, env)
      return Lisp::Debug.process_error("'load' requires 1 argument.", env) if args.empty?
      fname = args.car.evaluate(env)
      return Lisp::Debug.process_error("'load' requires a string argument.", env) unless fname.string?
      filename = fname.value.end_with?(".lsp") ? fname.value : "#{fname.value}.lsp"
      File.open(filename) do |f|
        contents = f.read()
        Lisp::Parser.new.parse_and_eval_all(contents)
      end
      Lisp::String.with_value("OK")
    end

    
    def self.load_library_impl(args, env)
      return Lisp::Debug.process_error("'load-library' requires 1 argument.", env) if args.empty?
      library_name = args.car.evaluate(env)
      return Lisp::Debug.process_error("'load-library' requires a string or symbol argument.", env) unless library_name.string? || library_name.symbol?
      Dir.chdir(File.join(App.documents_path, "libraries", "#{library_name}.lib")) do |d|
        if File.exists?("load.lsp")
          File.open("load.lsp") do |f|
            contents = f.read()
            Lisp::Parser.new.parse_and_eval_all(contents)
          end
        else
          Dir.glob("*.lsp") do |filename|
            File.open(filename) do |f|
              contents = f.read()
              Lisp::Parser.new.parse_and_eval_all(contents)
            end
          end
        end
      end
      Lisp::String.with_value("OK")
    end

    
    def self.load_project_impl(args, env)
      return Lisp::Debug.process_error("'load-project' requires 1 argument.", env) if args.empty?
      project_name = args.car.evaluate(env)
      return Lisp::Debug.process_error("'load-project' requires a string or symbol argument.", env) unless project_name.string? || project_name.symbol?
      Dir.chdir(File.join(App.documents_path, "projects", "#{project_name}.prj")) do |d|
        if File.exists?("load.lsp")
          File.open("load.lsp") do |f|
            contents = f.read()
            Lisp::Parser.new.parse_and_eval_all(contents)
          end
        else
          Dir.glob("*.lsp") do |filename|
            File.open(filename) do |f|
              contents = f.read()
              Lisp::Parser.new.parse_and_eval_all(contents)
            end
          end
        end
      end
      Lisp::String.with_value("OK")
    end
    
  end
end
