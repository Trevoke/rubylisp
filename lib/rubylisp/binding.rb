module Lisp

  class Binding
    attr_accessor :symbol, :value

    def initialize(symbol, value)
      @symbol = symbol
      @value = value
      self
    end

    def to_s
      "#{symbol.name} -> #{value.to_s}"
    end
  end

end
