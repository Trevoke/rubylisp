module Lisp

  class Equivalence

    def self.register
      Primitive.register("=", "(= number number)\n\nReturns whether the first numeric argument is equal to the second numeric argument.") do |args, env|
        Lisp::Equivalence::num_eq_impl(args, env)
      end
      
      Primitive.register("==", "(== number number)\n\nReturns whether the first numeric argument is equal to the second numeric argument.") do |args, env|
        Lisp::Equivalence::num_eq_impl(args, env)
      end
      
      Primitive.register("!=", "(!= number number)\n\nReturns whether the first numeric argument is not equal to the second numeric argument.") do |args, env|
        Lisp::Equivalence::num_neq_impl(args, env)
      end
      
      Primitive.register("/=", "(/= number number)\n\nReturns whether the first numeric argument is not equal to the second numeric argument.") do |args, env|
        Lisp::Equivalence::num_neq_impl(args, env)
      end

      Primitive.register("eq?", "(eq? sexpr sexpr)\n\nReturns whether the first argument is the same type as the second argument, and the same object, except in the case of numbers where the values are compared.") do |args, env|
        Lisp::Equivalence::eq_impl(args, env)
      end
      
      Primitive.register("eqv?", "(eqv? sexpr sexpr)\n\nReturns whether the first argument is the same type as the second argument, and the same object, except in the case of numbers where the values are compared.") do |args, env|
        Lisp::Equivalence::eqv_impl(args, env)
      end
      
      Primitive.register("equal?", "(equal? sexpr sexpr)\n\nReturns whether the first argument is the same type as the second argument, and are lists containing the same elements, are identical strings, are frames with matching slots, are numbers with the same value, or are the same object (if none of the above).") do |args, env|
        Lisp::Equivalence::equal_impl(args, env)
      end
      
    end

    # == - Check two integers for equivalence
    
    def self.num_eq_impl(args, env)
      raise "= and == need 2 arguments, received #{args.length}" if args.length != 2
      c1 = args.car.evaluate(env)
      c2 = args.cadr.evaluate(env)
      return Lisp::FALSE unless c1.integer? && c2.integer?
      Lisp::Boolean.with_value(c1.value == c2.value)
    end

    def self.num_neq_impl(args, env)
      raise "!= and /= needs 2 arguments, received #{args.length}" if args.length != 2
      c1 = args.car.evaluate(env)
      c2 = args.cadr.evaluate(env)
      return Lisp::TRUE unless c1.integer? && c2.integer?
      Lisp::Boolean.with_value(c1.value != c2.value)
    end


    # eq? - identity, typically used for symbols
    
    def self.eq_check(o1, o2)
      return Lisp::FALSE unless o1.type == o2.type
      Lisp::Boolean.with_value(case o1.type
                               when :number
                                 (o1.integer? == o2.integer?) && (o1.value == o2.value)
                               else
                                 o1.equal?(o2)
                               end)
    end
    

    def self.eq_impl(args, env)
      raise "eq? needs 2 arguments, received #{args.length}" if args.length != 2
      o1 = args.car.evaluate(env)
      o2 = args.cadr.evaluate(env)
      eq_check(o1, o2)
    end
    

    # eqv? - same as eq?

    def self.eqv_check(o1, o2)
      eq_check(o1, o2)
    end
    
    
    def self.eqv_impl(args, env)
      raise "eq? needs 2 arguments, received #{args.length}" if args.length != 2
      o1 = args.car.evaluate(env)
      o2 = args.cadr.evaluate(env)
      eqv_check(o1, o2)
    end
    

    # equal? - object equality: same value

    def self.equal_check(o1, o2)
      return Lisp::FALSE unless o1.type == o2.type
      Lisp::Boolean.with_value(case o1.type
                               when :pair
                                 o1.eq?(o2)
                               when :string
                                 o1.value.eql?(o2.value)
                               when :frame
                                 o1.eq?(o2)
                               when :number
                                 (o1.integer? == o2.integer?) && (o1.value == o2.value)
                               else
                                 o1.equal?(o2)
                               end)
    end

    
    def self.equal_impl(args, env)
      raise "equal? needs 2 arguments, received #{args.length}" if args.length != 2
      o1 = args.car.evaluate(env)
      o2 = args.cadr.evaluate(env)
      self.equal_check(o1, o2)
    end
  end

end
