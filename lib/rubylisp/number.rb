module Lisp

  class Number < Atom

    def self.with_value(n)
      self.new(n)
    end

    def initialize(n = 0)
      @value = n
    end

    def set!(n)
      @value = n
    end

    def number?
      true
    end

    def integer?
      @value.integer?
    end
    
    def float?
      !@value.integer?
    end
    
    def integer
      @value.to_i
    end

    def positive?
      @value > 0
    end

    def zero?
      @value == 0
    end

    def negative?
      @value < 0
    end

    def type
      :number
    end

    def to_s
      "#{@value}"
    end
    
    def true?
      @value != 0
    end

    def false?
      @value == 0
    end

  end

end
