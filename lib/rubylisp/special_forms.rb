# -*- coding: utf-8 -*-
module Lisp

  class SpecialForms

    def self.register
      Primitive.register("cond", 
                         "(cond (predicate sexpr...)... [(else sexpr...)])\n\nEach predicate is evaluated in order until one results in true value. The expressions associated with this predicate are then evaluated in order, and the result of the `cond` is the result of the last evaluation. If all predicates evaluate to false values, the value of `cond` in indeterminate. If, however, the final predicate is the symbol `else` the expressions associated with it are evaluated, and the value of `cond` is the value of the last evaluation.",
                         true) do |args, env|
                           Lisp::SpecialForms::cond_impl(args, env)
                         end

      Primitive.register("case", 
                         "(case target-sexpr ((value...) sexpr...)... [(else sexpr...)])\n\n`case` chooses code to evaluate based on the value of the target `sexpr`. Each condition clause is a list of possible values. A clause is selected if the target value is in it’s list of possible values. Any number of expressions can be associated with each target value.",
                         true) do |args, env| 
                           Lisp::SpecialForms::case_impl(args, env)
                         end
                         
      Primitive.register("if", 
                         "(if condition true-clause)\n(if condition true-clause false-clause)\n\n`if` has two forms, one that conditionally evaluates a single `sexpr` (see `begin` which provides a way to use multiple `sexprs` in this context) and one that chooses an `sexpr` to evaluate based on the value of the condition.\n\nIn the single action version, `nil` is the value of the form when the conditions evaluates to `false`. Note that it is preferable to use `when` (see below) instead of this form of `if`.",
                         true) do |args, env|
                           Lisp::SpecialForms::if_impl(args, env)
                         end
                         
      Primitive.register("when", 
                         "(when condition sexpr...)\n\nIff the condition evaluates to logically true, the `sexprs` are evaluated and the result of the last one is the result of the form, otherwise nil is the result.",
                         true) do |args, env|
                           Lisp::SpecialForms::when_impl(args, env)
                         end
                         
      Primitive.register("unless", 
                         "(unless condition sexpr...)\n\nIff the condition evaluates to logically false, the `sexprs` are evaluated and the result of the last one is the result of the form, otherwise nil is the result. This is the same functionally as `(when (not condition) sexpr...)` but is simpler and can be clearer.", 
                         true) do |args, env| 
                           Lisp::SpecialForms::unless_impl(args, env)
                         end
                         
      Primitive.register("lambda", 
                         "(lambda (param...) sexpr...)\n\nCreates an anonymous function. This can then be used in a function call.\n\n    ((lambda (x)\n      (+ x x))\n    5) ⇒ 10\n\nFunctions can be named (i.e. bound to a symbol) and later referred to by using define (but using `defun` is preferred):\n\n    (define foo (lambda (x)\n                  (+ x x)))\n    (foo 5) ⇒ 10\n\n`lambda` creates a local environment at the point of it’s evaluation. This environment is attached to the resulting function, and any binding or symbol lookup starts in this local environment.", 
                        true) do |args, env|
                          Lisp::SpecialForms::lambda_impl(args, env)
                        end
                        
      Primitive.register("define",
                         "(define symbol value)\n\nEvaluates the value expression and binds it to the symbol, returning the value.",
                         true) do |args, env| 
                           Lisp::SpecialForms::define_impl(args, env)
                         end
                         
      Primitive.register("defun",
                         "(defun (symbol param...) doc-string sexpr...)\n\nCreate a named function:\n\n`symbol` specifies the name (how you reference the function)\n\n`param...` parameters of the function, these are bound to the respective arguments when the function is called.\n\n`doc-string` is an optional documentation string.\n\n`sexpr...` the sequence of expressions that are evaluated in order when the function is called. The final evaluation result becomes the value of evaluation of the function.",
                         true) do |args, env| 
                           Lisp::SpecialForms::defun_impl(args, env)
                         end
                         
      Primitive.register("defmacro",
                         "(defmacro (symbol param...) sexpr)\n\nCreate a named macro:\n\n`symbol` specifies the name (how you reference the macro)\n\n`param...` parameters of the macro, these are bound to the respective arguments when the macro is invoked. **NOTE** that the arguments to a macro invokation are **not** evaluated, but are passed as is to the macro to do with as it wishes.\n\n`sexpr` the template expression that is processed when the macro is invoked. The result of evaluating the processed template expression becomes the value of the macro's invocation.", 
                         true) do |args, env|
                           Lisp::SpecialForms::defmacro_impl(args, env)
                         end
      
      Primitive.register("let", 
                         "(let ((name value)...) sexpr...)\n\nCreate a local scope and bindings for evaluating a body of code. The first argument is a list of bindings. Each binding is a raw symbol (doesn’t get evaluated) that is the name to be bound, and a value (which is evaluated). These bindings are added to a scope that is local to the let. The body is evaluated in this local scope. The value of the `let` is the last evaluation result. Bindings values are evaluated in the environment where the `let` is defined, and so are independant.", 
                         true) do |args, env|
                           Lisp::SpecialForms::let_impl(args, env)
                         end
                         
      Primitive.register("let*", 
                         "(let* ((name value)...) sexpr...)\n\nCreate a local scope and bindings for evaluating a body of code. The first argument is a list of bindings. Each binding is a raw symbol (doesn’t get evaluated) that is the name to be bound, and a value (which is evaluated). Each binding’s value is evaluated in the context of the local scope. I.e. bindings cascade. The body is evaluated in this local scope. The value of the `let*` is the last evaluation result.",
                         true) do |args, env|
                           Lisp::SpecialForms::letstar_impl(args, env)
                         end
                         
      Primitive.register("begin",
                         "(begin sexpr...)\n\nEvaluates the each `sexpr` in order, returning the result of the last one evaluated. Used in a context that allows a single `sexpr` but you need multiple.",
                         true) do |args, env|
                           Lisp::SpecialForms::begin_impl(args, env)
                         end
                         
      Primitive.register("do",
                         "(do ((name initial next)...) ((test sexpr...)) sexpr...)\n\nThis is a general purpose iteration construct. There are three sections:\n\nbindings\nThis is similar to `let` in that it defines names that can be used in the remainder of the scope of the `do`. Like `let` it is a list of lists, each starting with the binding name followed by the initial value of the binding. The difference is that this is followed by an expression that is evaluated at the beginning of each subsequent pass through the loop, and whose result is used as a new binding of the name.\n\ntermination\nThis is a list whose first element is an expression which is evaluated before each pass through the loop (after rebinding the variables). If it evaluates to a truthy value, the remaining expressions are evaluated in turn. The result of the final one is used as the value of the `do`.\n\nbody\nThis is a sequence of expressions that are evaluated in order each time though the loop. This section can be empty.",
                         true) do |args, env|
                           Lisp::SpecialForms::do_impl(args, env)
                         end
                         
      Primitive.register("eval",
                         "(eval sexpr)\n\nEvaluate `sexpr`.",
                         true) do |args, env|
                           Lisp::SpecialForms::eval_impl(args, env)
                         end
                         
      Primitive.register("apply",
                         "(apply function sexpr...)\n\nApply the function that results from evaluating `function` to the argument list resulting from evaluating each `sexpr`. Each initial `sexpr` can evaluate to any type of object, but the final one (and there must be at least one `sexpr`) must evaluate to a list.",
                         true) do |args, env|
                           Lisp::SpecialForms::apply_impl(args, env)
                         end
                         
      Primitive.register("=>",
                         "(=> value sexpr|symbol...)\n\nThis creates a cascade.\n\n`value` (evaluated once at the beginning) is used as the initial argument to **each** function, and they are independent and do not pass results one to another. `value` is the result of the form.\n\nSince this is implemented by syntactic modification, a `lambda` form **cannot** be used here as an `sexpr`.",
                         true) do |args, env|
                           Lisp::SpecialForms::tap_impl(args, env)
                         end
                         
      Primitive.register("->",
                         "(-> value sexpr|symbol...)\n\nThis creates a function chain. `value` (evaluated first) is used as the first argument to the first `sexpr`. The result of each `sexpr` is used as the first argument of the next, and the result of the final `sexpr` is the value of the `->` form. If a `sexpr` would take a single argument (which would be provided by the `value` or the result of the previous `sexpr`, just the function name can be used. Since this is implemented by syntactic modification, a `lambda` form cannot be used here as an `sexpr`.\n\nThe form `(-> 0 a b c)` is equivalent to `(c (b (a 0)))`.",
                         true) do |args, env|
                           Lisp::SpecialForms::chain_impl(args, env)
                         end
                         
      Primitive.register("quote", "(quote _expr_)\n\nSurpresses evaluation of the expression.\n\n         (quote (+ 1 2)) ⇒ (+ 1 2)\n\nThere is a shortcut for quote that uses the single quote:\n\n        '(+ 1 2) ⇒ (+ 1 2)") do |args, env|
        Lisp::SpecialForms::quote_impl(args, env)
      end
      
      Primitive.register("quasiquote", "(quasiquote _sexpr_)\n\nThis defines a template expression that can be filled in by unquote and unquote-splicing. The backquote character can be used as a shorthand for quasiquote: `sexpr.") do |args, env|
        Lisp::SpecialForms::quasiquote_impl(args, env)
      end
      
      Primitive.register("gensym", "(gensym)\n(gensym _prefix_)\n\nThis creates a unique symbol. If you provide the optional prefix string it is
used as the initial part of the symbol, otherwise GENSYM is used. gensym is
useful in macros when you need a unique name for something.") do |args, env|
        Lisp::SpecialForms::gensym_impl(args, env)
      end
      
      Primitive.register("expand", "(expand _symbol_ _sexpr_...)\n\nExpands the macro named by symbol, passing the evaluated sequence of sexpr as arguments.\n\nNOTE: whereas invoking the macro (in the same way you invoke a function) expands and evaluates, expand (as you would expect) only expands the macro, resulting in the expanded template sexpr. This can then be evaluated as desired.") do |args, env|
        Lisp::SpecialForms::expand_impl(args, env)
      end
      
      @@SYMBOL_COUNT = 0
    end

    def self.cond_impl(args, env)
      unless args.nil?
        args.each do |clause|
          body = clause.cdr
          if clause.car.to_s == "else"
            result = body.evaluate_each(env) unless body.nil?
            return result
          else
            condition = clause.car.evaluate(env)
            if condition.value
              result = body.evaluate_each(env) unless body.nil?
              return result
            end
          end
        end
      end
      nil
    end


    def self.case_impl(args, env)
      result = nil
      key_value = args.car.evaluate(env)
      args.cdr.each do |clause|
        if clause.pair?
          body = clause.cdr
          if clause.car.to_s == "else"
            result = body.evaluate_each(env) unless body.nil?
            return result
          elsif clause.car.any? {|item| item.eq?(key_value)}
            result = body.evaluate_each(env) unless body.nil?
            return result
          end
        else
          return Lisp::Debug.process_error("Case requires non-atomic clauses", env)
        end
      end
      return nil
    end


    def self.if_impl(args, env)
      return Lisp::Debug.process_error("IF requires a condition, true action, and possibly an else action", env) unless args.length == 2 || args.length == 3
      condition = args.car.evaluate(env)
      if condition.true?
        args.cadr.evaluate(env)
      elsif args.length == 3
        args.caddr.evaluate(env)
      else
        nil
      end
    end


    def self.when_impl(args, env)
      return Lisp::Debug.process_error("WHEN requires a condition and sexprs to evaluate.", env) unless args.length >= 2
      condition = args.car.evaluate(env)
      return args.cdr.evaluate_each(env) if condition.true?
      nil
    end


    def self.unless_impl(args, env)
      return Lisp::Debug.process_error("UNLESS requires a condition and sexprs to evaluate.", env) unless args.length >= 2
      condition = args.car.evaluate(env)
      return args.cdr.evaluate_each(env) unless condition.true?
      nil
    end


    def self.lambda_impl(args, env)
      arguments = args.car
      body = args.cdr
      Lisp::Function.new("lambda", arguments, "", body, env)
    end


    def self.define_variable(definition, value, env)
      return Lisp::Debug.process_error("Variable names must be literal symbols.", env) unless definition.symbol?

      ev = value.evaluate(env)
      Lisp::EnvironmentFrame.global.bind(definition, ev)
      ev
    end

    
    def self.define_function(definition, body, env)
      name = definition.car
      return Lisp::Debug.process_error("Function name must be a symbol", env) unless name.symbol?
      arguments = definition.cdr
      doc = nil
      if body.car.string?
        doc = body.car
        body = body.cdr
      end
      f = Lisp::Function.new(name, arguments, doc, body, env)
      env.bind_locally(name, f)
      f
    end


    def self.defun_impl(args, env)
      definition = args.car
      return Lisp::Debug.process_error("Function definition must specify name and parameters in a list", env) unless definition.list?
      define_function(definition, args.cdr, env)
    end
    
    
    def self.define_impl(args, env)
      definition = args.car
      if definition.list?
        define_function(definition, args.cdr, env)
      else
        return Lisp::Debug.process_error("A symbol can be bound to only a single value.", env) unless args.cdr.length == 1
        define_variable(definition, args.cadr, env)
      end
    end


    def self.defmacro_impl(args, env)
      return Lisp::Debug.process_error("defmacro requires 2 or 3 arguments: a name and argument list, and a template expression.", env) unless args.length == 2 || args.length == 3
      definition = args.car
      return Lisp::Debug.process_error("defmacro requires macro name and args in a list as it's first argument.", env) if definition.nil? || !definition.list?
      name = definition.car
      arguments = definition.cdr
      doc = nil
      if args.cadr.string?
        doc = args.cadr
        body = args.caddr
      else
        body = args.cadr
      end
      m = Lisp::Macro.new(name, arguments, doc, body, env)
      env.bind_locally(name, m)
      m
    end

    def self.quote_impl(args, env)
      args.car
    end


    def self.gensym_impl(args, env)
      return Lisp::Debug.process_error("gensym requires 0 or 1 argument", env) if args.length > 1
      prefix = if args.length == 0
                 "GENSYM"
               else
                 return Lisp::Debug.process_error("gensym's argument must be a string", env) unless args.car.string?
                 args.car.to_s
               end
      sym = Lisp::Symbol.named("#{prefix}-#{@@SYMBOL_COUNT}")
      @@SYMBOL_COUNT += 1
      sym
    end

    
    def self.expand_impl(args, env)
      macro = args.car.evaluate(env)
      return Lisp::Debug.process_error("The first argument to expand must be a macro", env) unless macro.macro?
      macro.expand(args.cdr, env, true)
    end
    

    def self.process_quasiquoted(sexpr, level, env)
      if !sexpr.list?
        ConsCell.cons(sexpr)
      elsif sexpr.car.symbol? && sexpr.car.name == "quasiquote"
        ConsCell.cons(ConsCell.cons(Symbol.named("quasiquote"), process_quasiquoted(sexpr.cadr, level + 1, env)))
      elsif sexpr.car.symbol? && sexpr.car.name == "unquote"
        if level == 1
          ConsCell.cons(process_quasiquoted(sexpr.cadr, level, env).car.evaluate(env))
        else
          ConsCell.cons(ConsCell.cons(Symbol.named("unquote"), process_quasiquoted(sexpr.cadr, level - 1, env)))
        end
      elsif sexpr.car.symbol? && sexpr.car.name == "unquote-splicing"
        if level == 1
          process_quasiquoted(sexpr.cadr, level, env).car.evaluate(env)
        else
          ConsCell.cons(ConsCell.cons(Symbol.named("unquote-splicing"), process_quasiquoted(sexpr.cadr, level - 1, env)))
        end
      else
        l = ConsCell.array_to_list(sexpr.to_a.map {|e| process_quasiquoted(e, level, env)}).flatten
        ConsCell.cons(l)
      end
    end
    

    def self.quasiquote_impl(args, env)
      return process_quasiquoted(args.car, 1, env).car
    end


    def self.do_let_bindings(bindings, binding_env, local_env)
      bindings.each do |binding_pair|
        return Lisp::Debug.process_error("let requires a list of bindings (that are 2 element lists) as it's first argument", env) unless binding_pair.list?
        name = binding_pair.car
        return Lisp::Debug.process_error("the first part of a let binding pair must be a symbol", env) unless name.symbol?
        value = binding_pair.cadr.evaluate(binding_env)
        local_env.bind_locally(name, value)
      end
    end
    

    def self.let_impl(args, env)
      bindings = args.car || Lisp::ConsCell.new
      return Lisp::Debug.process_error("let requires a list of bindings as it's firest argument", env) unless bindings.list?
      local_frame = EnvironmentFrame.extending(env)
      local_frame.previous = env
      do_let_bindings(bindings, env, local_frame)
      args.cdr.evaluate_each(local_frame)
    end


    def self.letstar_impl(args, env)
      bindings = args.car || Lisp::ConsCell.new
      return Lisp::Debug.process_error("let requires a list of bindings as it's firest argument", env) unless bindings.list?
      local_frame = EnvironmentFrame.extending(env)
      do_let_bindings(bindings, local_frame, local_frame)
      args.cdr.evaluate_each(local_frame)
    end


    def self.begin_impl(args, env)
      args.evaluate_each(env)
    end


    def self.do_impl(args, env)
      return Lisp::Debug.process_error("Do requires at least a list of bindings and a test clause", env) if args.length < 2
      bindings = args.car
      return Lisp::Debug.process_error("Do requires a list of bindings as it's first argument", env) unless bindings.list?
      test_clause = args.cadr
      return Lisp::Debug.process_error("Do requires a list of termination condition and result expressions as it's second argument", env) unless test_clause.list?
      body = args.cddr

      local_frame = EnvironmentFrame.extending(env)
      local_frame.previous = env

      bindings.each do |binding|
        return Lisp::Debug.process_error("do bindings must be (name initial next)", env) unless binding.list?
        name = binding.car
        return Lisp::Debug.process_error("binding name must be a symbol", env) unless name.symbol?
        value = binding.cadr.evaluate(local_frame)
        local_frame.bind_locally(name, value)
      end

      while true do
        if test_clause.car.evaluate(local_frame).value
          result = nil
          test_clause.cdr.each {|sexpr| result = sexpr.evaluate(local_frame) } unless test_clause.cdr.nil?
          return result
        end

        body.each {|sexpr| sexpr.evaluate(local_frame) } unless body.nil?

        bindings.each do |binding|
          unless binding.caddr.nil?
            value = binding.caddr.evaluate(local_frame)
            local_frame.bind_locally(binding.car, value)
          end
        end
      end
    end


    def self.eval_impl(args, env)
      return Lisp::Debug.process_error("eval expects a single argument, received #{args.length}.", env) if args.length != 1
      arg = args.car.evaluate(env)
      return Lisp::Debug.process_error("eval expect a list argument, received a #{arg.type}.", env) unless arg.list?
      arg.evaluate(env)
    end


    def self.apply_impl(args, env)
      func = args.car.evaluate(env)
      return Lisp::Debug.process_error("Expected #{args.car} to evaluate to a function.", env) unless func.primitive? || func.function?

      a = args.cdr.to_a.collect {|sexpr| sexpr.evaluate(env)}
      arg_list = if a[-1].list?
                   Lisp::ConsCell.array_to_list(a[0..-2], a[-1])
                 else
                   args.cdr
                 end
      func.apply_to(arg_list, env)
    end


    def self.chain_impl(args, env)
      return Lisp::Debug.process_error("-> requires at the very least an initial value.", env) unless args.length > 0
      value = args.car.evaluate(env)
      cell = args.cdr
      while !cell.nil?
        sexpr = cell.car
        
        new_expr = if sexpr.list?
                     Lisp::ConsCell.cons(sexpr.car, Lisp::ConsCell.cons(value, sexpr.cdr))
                   else
                     Lisp::ConsCell.array_to_list([sexpr, value])
                   end
        value = new_expr.evaluate(env)
        cell = cell.cdr
      end
      value
    end


    def self.tap_impl(args, env)
      return Lisp::Debug.process_error("tap requires at the very least an initial value.", env) unless args.length > 0
      value = args.car.evaluate(env)
      cell = args.cdr
      while !cell.nil?
        sexpr = cell.car
        new_expr = if sexpr.list?
                     Lisp::ConsCell.cons(sexpr.car, Lisp::ConsCell.cons(value, sexpr.cdr))
                   else
                     Lisp::ConsCell.array_to_list([sexpr, value])
                   end
        new_expr.evaluate(env)
        cell = cell.cdr
      end
      value
    end


  end

end
