module Lisp

  class TypeChecks

    def self.register
      Primitive.register("list?")     {|args, env| Lisp::TypeChecks::typep_impl("list?", :pair, args, env) }
      Primitive.register("pair?")     {|args, env| Lisp::TypeChecks::typep_impl("pair?", :pair, args, env) }
      Primitive.register("symbol?")   {|args, env| Lisp::TypeChecks::typep_impl("symbol?", :symbol, args, env) }
      Primitive.register("number?")   {|args, env| Lisp::TypeChecks::typep_impl("number?", :number, args, env) }
      Primitive.register("integer?")  {|args, env| Lisp::TypeChecks::integerp_impl(args, env) }
      Primitive.register("float?")    {|args, env| Lisp::TypeChecks::floatp_impl(args, env) }
      Primitive.register("function?") {|args, env| Lisp::TypeChecks::functionp_impl(args, env) }

      Primitive.register("nil?")      {|args, env| Lisp::TypeChecks::nilp_impl(args, env) }
      Primitive.register("null?")     {|args, env| Lisp::TypeChecks::nilp_impl(args, env) }
      Primitive.register("not-nil?")  {|args, env| Lisp::TypeChecks::not_nilp_impl(args, env) }
      Primitive.register("not-null?") {|args, env| Lisp::TypeChecks::not_nilp_impl(args, env) }
    end


    def self.typep_impl(name, sym, args, env)
      return Lisp::Debug.process_error("#{name} needs 1 argument", env) unless args.length == 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).type == sym)
    end


    def self.integerp_impl(args, env)
      return Lisp::Debug.process_error("integer? needs 1 argument", env) unless args.length == 1
      val = args.car.evaluate(env)
      return Lisp::Boolean.with_value(val.type == :number && val.integer?)
    end


    def self.floatp_impl(args, env)
      return Lisp::Debug.process_error("float? needs 1 argument", env) unless args.length == 1
      val = args.car.evaluate(env)
      return Lisp::Boolean.with_value(val.type == :number && val.float?)
    end


    def self.functionp_impl(args, env)
      return Lisp::Debug.process_error("function? needs 1 argument", env) unless args.length == 1
      val = args.car.evaluate(env)
      return Lisp::Boolean.with_value(val.type == :function || val.type == :primitive)
    end


    def self.nilp_impl(args, env)
      return Lisp::Debug.process_error("nil? needs 1 argument", env) unless args.length == 1
      return Lisp::Boolean.with_value(args.car.evaluate(env).nil?)
    end


    def self.not_nilp_impl(args, env)
      return Lisp::Debug.process_error("not-nil? needs 1 argument", env) unless args.length == 1
      return Lisp::Boolean.with_value(!args.car.evaluate(env).nil?)
    end

  end
end
