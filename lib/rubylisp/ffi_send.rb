module Lisp

  class FfiSend < Atom

    def initialize(name)
      @value = name.to_sym
    end

    def apply_to(args, env)
      apply_to_without_evaluating(Lisp::ConsCell.array_to_list(args.to_a.map {|a| a.evaluate(env)}), env)
    end

    def convert_value(value)
      case value.class.name
      when "Fixnum", "Float"
        Lisp::Number.with_value(value)
      when "TrueClass"
        Lisp::Boolean.TRUE
      when "FalseClass"
        Lisp::Boolean.FALSE
      when "String"
        Lisp::String.with_value(value)
      when "Symbol"
        Lisp::Symbol.named(value)
      when "Array"
        Lisp::ConsCell.array_to_list(value.map {|a| convert_value(a)})
      else
        Lisp::NativeObject.with_value(value)
      end
    end


    def process_arg(a, env)
      if a.function?
        proc do |*args|
          arg_list = args.empty? ? nil : Lisp::ConsCell.array_to_list(args.collect {|arg| convert_value(arg) })
          a.apply_to(arg_list, env)
        end
      elsif a.list?
        a.to_a.map {|i| process_arg(i, env)}
      else
        a.value
      end
    end
    

    def apply_to_without_evaluating(args, env)
      target = args.car
      return Lisp::Debug.process_error("Send target of '#{@value}' evaluated to nil.", env) if target.nil?
      return Lisp::Debug.process_error("Target of an FFI send of '#{@value}' must be a wrapped ObjC object, was #{target}", env) unless target.object?

      arguments = args.cdr.nil? ? [] : args.cdr.to_a.map {|a| process_arg(a, env)}      
      result = nil
      
      begin
        result = if arguments[-1].instance_of?(Proc)
                   target.value.send(@value, *(arguments[0..-2]), &arguments[-1])
                 else
#                   puts "Sending #{@value} with #{arguments}"
                   target.value.send(@value, *arguments)
                 end
      rescue Exception => e
        return Lisp::Debug.process_error("Exception sending #{@value}: #{e}", env)
      end

      convert_value(result)
    end

    def to_s
      ".#{@value}"
    end

    def primitive?
      true
    end

    def type
      :primitive
    end

  end

end
