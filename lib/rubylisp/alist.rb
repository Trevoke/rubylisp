module Lisp

  class AList < Object
    include Enumerable

    def self.register
      Primitive.register("acons")         {|args, env| Lisp::AList::acons_impl(args, env) }
      Primitive.register("assoc")         {|args, env| Lisp::AList::assoc_impl(args, env) }
      Primitive.register("rassoc")        {|args, env| Lisp::AList::rassoc_impl(args, env) }
      Primitive.register("dissoc")        {|args, env| Lisp::AList::dissoc_impl(args, env) }
      Primitive.register("zip")           {|args, env| Lisp::AList::zip_impl(args, env) }
      Primitive.register("alist-to-list") {|args, env| Lisp::AList::alist_to_list_impl(args, env) }
      Primitive.register("list-to-alist") {|args, env| Lisp::AList::list_to_alist_impl(args, env) }
    end

    def self.acons_impl(args, env)
      return Lisp::Debug.process_error("acons require at least 2 or 3 arguments", env) unless args.length == 2 || args.length == 3
      key = args.car.evaluate(env)
      value = args.cadr.evaluate(env)
      alist = args.length == 2 ? nil : args.caddr.evaluate(env)
      alist = Lisp::AList.from_list(alist) if !alist.nil? && alist.list? && !alist.alist?
      return Lisp::Debug.process_error("the last argument to acons has to be an association list", env) unless alist.nil? || alist.alist?

      if alist.nil?
        Lisp::AList.new({key => value})
      else
        alist.acons(key, value)
      end
    end

    def self.assoc_impl(args, env)
      return Lisp::Debug.process_error("assoc require 2 arguments", env) unless args.length == 2
      key = args.car.evaluate(env)
      alist = args.cadr.evaluate(env)
      alist = Lisp::AList.from_list(alist) if alist.list? && !alist.alist?
      return Lisp::Debug.process_error("the last argument to assoc has to be an association list", env) unless alist.alist?
      alist.assoc(key)
    end

    def self.rassoc_impl(args, env)
      return Lisp::Debug.process_error("assoc require 2 arguments", env) unless args.length == 2
      value = args.car.evaluate(env)
      alist = args.cadr.evaluate(env)
      alist = Lisp::AList.from_list(alist) if alist.list? && !alist.alist?
      return Lisp::Debug.process_error("the last argument to rassoc has to be an association list", env) unless alist.alist?
      alist.rassoc(value)
    end

    def self.dissoc_impl(args, env)
      return Lisp::Debug.process_error("assoc require 2 arguments", env) unless args.length == 2
      key = args.car.evaluate(env)
      alist = args.cadr.evaluate(env)
      alist = Lisp::AList.from_list(alist) if alist.list? && !alist.alist?
      return Lisp::Debug.process_error("the last argument to dissoc has to be an association list", env) unless alist.alist?
      alist.dissoc(key)
    end

    def self.zip_impl(args, env)
      return Lisp::Debug.process_error("assoc require 2 or 3arguments", env) unless args.length == 2 || args.length == 3
      key_list = args.car.evaluate(env)
      return Lisp::Debug.process_error("the keys supplied to zip has to be a list", env) unless key_list.list?
      value_list = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("the values supplied to zip has to be a list", env) unless value_list.list?
      return Lisp::Debug.process_error("zip requires the same number of keys and values", env) unless key_list.length == value_list.length

      if args.length == 3
        alist = args.caddr.evaluate(env)
        alist = Lisp::AList.from_list(alist) if alist.list? && !alist.alist?
        return Lisp::Debug.process_error("the third argument to zip has to be an association list", env) unless alist.alist?
        alist.zip(key_list.to_a, value_list.to_a)
      else
        Lisp::AList.zip(key_list.to_a, value_list.to_a)
      end
    end

    def self.alist_to_list_impl(args, env)
      return Lisp::Debug.process_error("alist-to-list requires 1 arguments", env) unless args.length == 1
      alist = args.car.evaluate(env)
      return alist if alist.list? && !alist.alist?
      return Lisp::Debug.process_error("the argument to alist-to-list has to be an association list", env) unless alist.alist?
            
      alist.to_list
    end
    
    def self.list_to_alist_impl(args, env)
      return Lisp::Debug.process_error("list-to-alist requires 1 arguments", env) unless args.length == 1
      list = args.car.evaluate(env)
      return Lisp::Debug.process_error("the argument to list-to-alist has to be a list", env) unless list.list?
            
      Lisp::AList.from_list(list)
    end

    
    def initialize(h=nil)
      @value = h || {}
    end

    def lisp_object?
      true
    end
    
    def eq?(sexpr)
      return false unless sexpr.alist?
      @value == sexpr.value
    end
    
    def empty?
      @value.empty?
    end

    def string?
      false
    end

    def character?
      false
    end

    def number?
      false
    end

    def positive?
      false
    end

    def zero?
      false
    end

    def negative?
      false
    end

    def symbol?
      false
    end

    def primitive?
      false
    end

    def function?
      false
    end

    def macro?
      false
    end

    def pair?
      true
    end

    def list?
      true
    end

    def alist?
      true
    end

    def frame?
      false
    end
    
    def length
      return @value.length
    end

    def car
      @value.first
    end

    def cdr
    end

    def acons(k, v)
      @value[k] = v
      self
    end

    def assoc(k)
      v = @value[k]
      Lisp::ConsCell.cons(k, v)
    end

    def rassoc(value)
      @value.each do |k, v|
        return Lisp::ConsCell.cons(k, v) if value == v || value.eq?(v)
      end
    end

    def dissoc(k)
      @value.delete(k)
      self
    end

    def zip(keys, values)
      keys.zip(values).each {|k, v| @value[k] = v}
      self
    end
    
    def self.zip(keys, values)
      self.new.zip(keys, values)
    end

    def self.from_list(list)
      h = {}
      list.each do |pair|
        h[pair.car] = pair.cdr
      end
      self.new(h)
    end

    def to_list
      Lisp::ConsCell.array_to_list(@value.map {|k, v| Lisp::ConsCell.cons(k, v)})
    end

    def to_s
      to_list.to_s
    end

   def print_string
      self.to_s
    end

  end

end
