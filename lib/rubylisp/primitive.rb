module Lisp

  class Primitive < Atom
    
    attr_reader :doc, :name
    
    def self.register(name, doc="", special=false, env=Lisp::EnvironmentFrame.global, &implementation)
      instance = self.new(name, doc, special, &implementation)
      env.bind(Symbol.named(name), instance)
    end

    def initialize(name, doc, special, &implementation)
      @name = name
      @doc = doc
      @special = special
      @implementation = implementation
    end

    def apply_to(args, env)
      @implementation.call(args, env)
    end

    def apply_to_without_evaluating(args, env)
      @implementation.call(args, env)
    end

    def to_s
      "<prim: #{@name}>"
    end

    def primitive?
      true
    end
    
    def special?
      @special
    end

    def type
      :primitive
    end

  end

end
