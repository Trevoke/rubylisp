module Lisp

  class Assignment

    def self.register
      Primitive.register("set!", "(set! _name_ _new-value_)\n\nThe way to assign (i.e. rebind) a symbol. `name` is the symbol to be rebound.
The `new-value` sexpr is evaluated to arrive at the new value to be bound to. Use of `set!` is frowned upon, and should not be used without thought.") do |args, env|
        Lisp::Assignment::setbang_impl(args, env)
      end
      
      Primitive.register("set-car!", "(set-car! _cons-cell_ _new-value_)\n\nSet the `car` pointer of `cons-cell`.") do |args, env|
        Lisp::Assignment::setcarbang_impl(args, env)
      end
      
      Primitive.register("set-cdr!", "(set-cdr! _cons-cell_ _new-value_)\n\nSet the `cdr` pointer of `cons-cell`.") do |args, env|
        Lisp::Assignment::setcdrbang_impl(args, env)
      end
      
      Primitive.register("set-nth!", "(set-nth! _n_ _list-or-vector_ _new-value_)\n\nSets the `n`th element of `list-or-vector` to `new-value`.") do |args, env|
        Lisp::Assignment::setnthbang_impl(args, env)
      end
      
    end

    
    def self.setbang_impl(args, env)
      sym = args.car
      return Lisp::Debug.process_error("set! requires a raw (unevaluated) symbol as it's first argument.", env) unless sym.symbol?
      value = args.cadr.evaluate(env)
      env.set(sym, value)
    end

    
    def self.setcarbang_impl(args, env)
      pair = args.car.evaluate(env)
      return Lisp::Debug.process_error("set-car! requires a pair as it's first argument.", env) unless pair.pair?
      value = args.cadr.evaluate(env)
      pair.set_car!(value)
    end

    
    def self.setcdrbang_impl(args, env)
      pair = args.car.evaluate(env)
      return Lisp::Debug.process_error("set-cdr! requires a pair as it's first argument.", env) unless pair.pair?
      value = args.cadr.evaluate(env)
      pair.set_cdr!(value)
    end


    def self.setnthbang_impl(args, env)
      return Lisp::Debug.process_error("set-nth! requires 3 arguments.", env) unless args.length == 3
      n = args.car.evaluate(env)
      return Lisp::Debug.process_error("The first argument of set-nth! has to be an number.", env) unless n.number?
      return Lisp::Debug.process_error("The first argument of set-nth! has to be positive.", env) unless n.value > 0

      l = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("set-nth! requires a list or vector as it's first argument.", env) unless l.list? || l.vector?
      value = args.caddr.evaluate(env)
      l.set_nth!(n.value, value)
      l
    end


  end
end
