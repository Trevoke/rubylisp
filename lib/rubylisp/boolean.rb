module Lisp

  class Boolean < Atom
    def self.TRUE
      @true_constant ||= Boolean.new(true)
    end

    def self.FALSE
      @false_constant ||= Boolean.new(false)
    end

    def self.with_value(b)
      b ? self.TRUE : self.FALSE
    end

    def initialize(b)
      @value = b
    end

    def type
      :boolean
    end

    def boolean?
      true
    end

    def to_s
      return "#t" if @value
      "#f"
    end

    def true?
      @value
    end

    def false?
      !@value
    end

    def negate
      Lisp::Boolean.with_value(!@value)
    end
    
  end

  TRUE = Boolean.TRUE
  FALSE = Boolean.FALSE
end
