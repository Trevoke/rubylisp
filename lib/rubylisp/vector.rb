module Lisp

  class Vector < Atom

    def self.register
      Primitive.register("make-vector")   {|args, env| Lisp::Vector::make_vector_impl(args, env) }
      Primitive.register("vector")   {|args, env| Lisp::Vector::vector_impl(args, env) }
    end

    def self.make_vector_impl(args, env)
      c = args
      a = []
      while !c.nil?
        a << c.car.evaluate(env)
        c = c.cdr
      end

      Lisp::Vector.new(a)
    end
  

    def self.vector_impl(args, env)
      return Lisp::Debug.process_error("vector requires a single list argument.", env) unless args.length == 1
      
      c = args.car.evaluate(env)
      Lisp::Vector.new(c.to_a)
    end
  

    def self.with_array(a)
      self.new(a)
    end

    
    def initialize(a = [])
      @value = a
      self
    end


    def type
      :vector
    end


    def vector?
      true
    end



    def empty?
      @value.empty?
    end


    def length
      @value.size
    end


    def add(e)
      @value << e
    end


    def to_a
      @value
    end
    

    def to_s
      "[#{@value.join(' ')}]"
    end


    def at(n)
      @value[n - 1]
    end
    alias_method :nth, :at


    def nth_tail(n)
      return Lisp::Vector.new if n > @value.size
      Lisp::Vector.new(@value[(n - 1)..-1])
    end
    
    
    def at_put(n, d)
      @value[n - 1] = d
    end


    def set_nth!(n, d)
      at_put(n, d)
    end


    def eq?(other)
      return false unless other.vector?
      return false unless @value.size == other.value.size
      (0..@value.size).each do |i|
        return false unless Lisp::Equivalence.equal_check(other.value[i], value[i]).value
      end
      true
    end

    def each &block
      @value.each &block
    end

  end

end
