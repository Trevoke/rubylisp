module Lisp

  class Logical

    def self.register
      Primitive.register("or",
                         "(or expression ...\n\nThe expressions are evaluated from left to right, and the value of the first expression that evaluates to a true value is returned. Any remaining expressions are not evaluated. If all expressions evaluate to false values, the value of the last expression is returned. If there are no expressions then #f is returned.",
                         true)  do |args, env|
                           Lisp::Logical::or_impl(args, env)
                         end
      Primitive.register("and",
                         "(and expression ...)\n\nThe expressions are evaluated from left to right, and the value of the first expression that evaluates to a false value is returned. Any remaining expressions are not evaluated. If all the expressions evaluate to true values, the value of the last expression is returned. If there are no expressions then #t is returned.",
                         true) do |args, env|
                           Lisp::Logical::and_impl(args, env)
                         end
      Primitive.register("not") {|args, env| Lisp::Logical::not_impl(args, env) }
    end


    def self.or_impl(args, env)
      return Lisp::Debug.process_error("or needs at least 2 arguments", env) unless args.length > 1
      value = !!args.inject(false) {|acc, item| acc || item.evaluate(env).value}
      return Lisp::Boolean.with_value(value)
    end

    def self.and_impl(args, env)
      return Lisp::Debug.process_error("and needs at least 2 arguments", env) unless args.length > 1
      value = !!args.inject(true) {|acc, item| acc && item.evaluate(env).value}
      return Lisp::Boolean.with_value(value)
    end

    def self.not_impl(args, env)
      return Lisp::Debug.process_error("not needs a single argument", env) unless args.length == 1
      return Lisp::Boolean.with_value(!(args.car.evaluate(env).value))
    end

  end
end
