module Lisp

  class Initializer

    def self.initialize_global_environment
      Lisp::EnvironmentFrame.global.bind(Symbol.named("nil"), nil)
    end

    def self.register_builtins
      Lisp::Equivalence.register
      Lisp::Math.register
      Lisp::Logical.register
      Lisp::SpecialForms.register
      Lisp::ListSupport.register
      Lisp::Relational.register
      Lisp::TypeChecks.register
      Lisp::Assignment.register
      Lisp::Testing.register
      Lisp::IO.register
      Lisp::AList.register
      Lisp::Frame.register
      Lisp::Character.register
      Lisp::String.register
      Lisp::NativeObject.register
      Lisp::ClassObject.register
      Lisp::System.register
      Lisp::Vector.register
      Lisp::Debug.register
    end
  end

end
