module Lisp

  class Testing
    

    def self.register
      Primitive.register("describe") {|args, env| Lisp::Testing::describe_impl(args, env) }
      Primitive.register("check")    {|args, env| Lisp::Testing::check_impl(args, env) }
      Primitive.register("check!")   {|args, env| Lisp::Testing::check_not_impl(args, env) }
      Primitive.register("check*")   {|args, env| Lisp::Testing::check_star_impl(args, env) }
    end

    def self.describe_impl(args, env)
      return Lisp::Debug.process_error("First arg to describe must be a string or symbol", env) if !args.car.symbol? && !args.car.string?
      return if args.cdr.nil?
      puts
      puts "  #{args.car.to_s}"
      args.cdr.each do |clause|
        clause.evaluate(env)
      end
    end

    def self.do_comparison(c1, c2)
      return Lisp::TRUE if c1.nil? && c2.nil?
      return Lisp::FALSE if c1.nil? || c2.nil?
      Lisp::Boolean.with_value(c1.eq?(c2))
    end

    def self.eval_check(code, c1, c2, inverted)
      check_result = self.do_comparison(c1, c2)
      passed = inverted ? check_result.negate : check_result
      
      if passed.true?
        @@number_of_passes += 1
        puts "ok"
      else
        @@number_of_fails += 1
        message = "failed: #{code.print_string} is #{c1.print_string}, expected #{inverted ? 'not ' : ''}#{c2.print_string}"
        puts message
        @@failure_messages << message
      end
    end


    def self.unary_check(name,sexpr, env, inverted)
      print "    (#{name} #{sexpr.print_string}) - "
      c1 = sexpr.evaluate(env)
      self.eval_check(sexpr, c1, inverted ? Lisp::FALSE : Lisp::TRUE, false)
    end


    def self.binary_check(name, sexpr_1, sexpr_2, env, inverted)
      print "    (#{name} "
      print "#{sexpr_1.print_string} "
      print "#{sexpr_2.print_string}) - "
      c1 = sexpr_1.evaluate(env)
      c2 = sexpr_2.evaluate(env)
      self.eval_check(sexpr_1, c1, c2, inverted)
    end
    
    
    def self.check_impl(args, env, inverted=false)
      @@number_of_tests += 1
      name = inverted ? "check!" : "check"
      Lisp::Boolean.with_value(case (args.length)
                               when 1
                                   self.unary_check(name, args.car, env, inverted)
                               when 2
                                   self.binary_check(name, args.car, args.cadr, env, inverted)
                               else
                                   return Lisp::Debug.process_error("check takes 1 or 2 arguments, received #{args.length}", env)
                               end)
    end

    
    def self.check_not_impl(args, env)
      self.check_impl(args, env, true)
    end
    
    
    def self.check_star_impl(args, env)
      return Lisp::Debug.process_error("check* needs 2 arguments, received #{args.length}", env) if args.length != 2
      @@number_of_tests += 1
      print "    (check* #{args.car.print_string} #{args.cadr.print_string}) - "
      
      c1 = args.car.evaluate(env)
      c2 = args.cadr
      self.eval_check(args.car, c1, c2, false)
    end
    
    def self.init
      @@number_of_tests = 0
      @@number_of_fails = 0
      @@number_of_passes = 0
      @@number_of_errors = 0
      @@failure_messages = []
      @@error_messages = []
    end
    
    def self.dump_messages(header, messages)
      return if messages.empty?
      puts "  #{header}:"
      messages.each do |message|
        puts "    #{message}"
      end
      puts ""
    end
    
    def self.print_test_results
      puts ""
      puts "  Done."
      puts ""
      
      dump_messages("Errors", @@error_messages)
      dump_messages("Failures", @@failure_messages)
      
      puts "  #{@@number_of_tests} Lisp tests"
      puts "  #{@@number_of_passes} passes, #{@@number_of_fails} fails, #{@@number_of_errors} errors"
    end
  
    def self.run_tests
      register
      init
      Dir[File.dirname(__FILE__) + '/../../lisptest/*_test.lsp'].each do |test_filename|
        puts "\nLoading #{test_filename}"
        File.open(test_filename) do |f|
          code_string = f.read()
          Parser.new.parse_and_eval_all(code_string)
        end
      end
      print_test_results
      @@number_of_errors == 0 && @@number_of_fails == 0
    end

  end
end
