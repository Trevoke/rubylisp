module Lisp

  class Frame < Atom

    def self.register
      Primitive.register("make-frame", "(make-frame slot-name slot-value ... )\n\nFrames can be created using the make-frame function, passing it an alternating sequence of slot names and values:\n\n        (make-frame a: 1 b: 2)\n\nThis results in a frame with two slots, named a: and b: with values 1 and 2, respectively.") do |args, env|
        Lisp::Frame::make_frame_impl(args, env)
      end
      
      Primitive.register("has-slot?", "(has-slot? frame slot-name)\n\nThe has-slot? function is used to query whether a frame contains (directly or in an ancestor) the particular slot.") do |args, env|
        Lisp::Frame::has_slot_impl(args, env)
      end
      
      Primitive.register("get-slot", "(get-slot _frame_ _slot-name_)\n\nThe get-slot function is used to retrieve values from frame slots") do |args, env|
        Lisp::Frame::get_slot_impl(args, env)
      end
      
      Primitive.register("get-slot-if", "(get-slot-if frame slot-name)\n\nThe same as above, except that if a matching slot is not found, nil is returned instead of raising an error.") do |args, env|
        Lisp::Frame::get_slot_if_impl(args, env)
      end
      
      Primitive.register("remove-slot!", "(remove-slot! frame slot-name)\n\nThe remove-slot! function is used to function is used to remove a slot from a frame. It only removes slots from the frame itself. not any of it's parents. remove-slot! return #t if the slot was removed, #f otherwise.") do |args, env|
        Lisp::Frame::remove_slot_impl(args, env)
      end
      
      Primitive.register("set-slot!", "(set-slot! frame slot-name new-value)\n\nThe set-slot! function is used to change values in frame slots") do |args, env|
        Lisp::Frame::set_slot_impl(args, env)
      end
      
      Primitive.register("send", "(send frame slot-name arg...)\n\nSend the message slot-name to frame, passing along the arg collection. The result is what is returned by the code in that slot.") do |args, env|
        Lisp::Frame::send_impl(args, env)
      end
      
      Primitive.register("send-super", "**(send-super slot-name arg...)\n\nLike send, but sends to the first parent that has the named slot. send-super can only be used from within a frame.") do |args, env|
        Lisp::Frame::send_super_impl(args, env)
      end
      
      Primitive.register("clone", "(clone frame)\n\nFrames represent things. For example, you could use a frame that looks like {x: 1 y: 10} to represent a point. A system that would use point frames will typically need many independant points. The approach to this is to create a prototypical point data frame, and use the clone function to create individual, independant frames.") do |args, env|
        Lisp::Frame::clone_impl(args, env)
      end
      
      Primitive.register("keys", "(keys frame)\n\nReturn a list of the keys in the frame.") do |args, env|
        Lisp::Frame::keys_impl(args, env)
      end
      
    end


    def self.make_frame_impl(args, env)
      c = args
      m = {}
      while !c.nil?
        k = c.car
        return Lisp::Debug.process_error("Slot names must be a symbol, found a {k.type}.", env) unless k.symbol?
        return Lisp::Debug.process_error("Slot names must end in a colon, found '#{k}'.", env) unless k.naked?
        v = c.cadr.evaluate(env)
        m[k] = v
        c = c.cddr
      end

      Lisp::Frame.with_map(m)
    end


    def self.has_slot_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      key = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Frame key must be a symbol but was #{key.type}.", env) unless key.symbol?
      return Lisp::TRUE if frame.has_slot?(key)
      Lisp::FALSE
    end

    
    def self.get_slot_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      key = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Frame key (#{key.to_s}) must be a symbol but was #{key.type}.", env) unless key.symbol?
      return Lisp::Debug.process_error("Frame key (#{key.to_s}) must name an existing slot.", env) unless frame.has_slot?(key)
      frame.get(key)
    end

    
    def self.get_slot_if_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      key = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Frame key (#{key.to_s}) must be a symbol but was #{key.type}.", env) unless key.symbol?
      frame.get(key)
    end

    
    def self.remove_slot_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      key = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Frame key (#{key.to_s}) must be a symbol but was #{key.type}.", env) unless key.symbol?
      return Lisp::TRUE if frame.remove(key)
      Lisp::FALSE
    end

    
    def self.set_slot_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      key = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Frame key (#{key.to_s}) must be a symbol but was #{key.type}.", env) unless key.symbol?
      value = args.caddr.evaluate(env)
      frame.at_put(key, value)
    end
    

    def self.send_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      selector = args.cadr.evaluate(env)
      return Lisp::Debug.process_error("Selector must be a symbol but was #{selector.type}.", env) unless selector.symbol?
      return Lisp::Debug.process_error("Message sent must name an existing slot in the receiver.", env) unless frame.has_slot?(selector)
      func = frame.get(selector)
      return Lisp::Debug.process_error("Message sent must select a function slot but was #{func.type}.", env) unless func.function?
      params = args.cddr
      frame_env = Lisp::EnvironmentFrame.extending(env, frame)
      frame_env.bind_locally(Symbol.named("self"), frame)
      func.apply_to(params, frame_env)
    end

    def self.get_super_function(selector, env)
      f = env.frame
      return nil if f.nil?
      f.parents.each do |p|
        func = p.get(selector)
        return func unless func.nil?
      end
      nil
    end
    
    def self.send_super_impl(args, env)
      return Lisp::Debug.process_error("super can only be used within the context of a frame.", env) unless env.frame
      selector = args.car.evaluate(env)
      return Lisp::Debug.process_error("Selector must be a symbol but was #{selector.type}.", env) unless selector.symbol?
      func = get_super_function(selector, env)
      return Lisp::Debug.process_error("Message sent must select a function slot but was #{func.type}.", env) unless func && func.function?
      params = args.cdr
      frame_env = Lisp::EnvironmentFrame.extending(env, env.frame)
      frame_env.bind_locally(Symbol.named("self"), env.frame)
      func.apply_to(params, frame_env)
    end

    
    def self.clone_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      frame.clone
    end


    def self.keys_impl(args, env)
      frame = args.car.evaluate(env)
      return Lisp::Debug.process_error("Frame data must be a frame but was #{frame.type}.", env) unless frame.frame?
      ConsCell.array_to_list(frame.value.keys)
    end


    def self.with_map(m)
      self.new(m)
    end

    def initialize(m=nil)
      @value = m || {}
    end


    def clone
      Lisp::Frame.with_map(@value.clone)
    end

    
    def is_parent_key(k)
      k.to_s[-2] == "*"
    end


    def local_slots
      @value.keys
    end


    def inherited_value_slots
      parent_frames = parent_slots.collect {|pk| get(pk)}
      parent_slots = parent_frames.collect {|p| p.inherited_value_slots}
      local_value_slots = Set[local_slots.reject {|s| is_parent_key(k)}]
      parent_slots.inject(local_value_slots) {|all, s| all + s} 
    end
    
    
    def has_parent_slots?
      @value.keys.any? {|k| is_parent_key(k)}
    end


    def parent_slots
      @value.keys.select {|k| is_parent_key(k)}
    end


    def parents
      parent_slots.collect {|pk| @value[pk]}
    end

    
    def has_slot_locally?(n)
      @value.has_key?(n)
    end


    def has_slot_helper(n, v)
      return false if v.include?(self)
      v << self
      return true if has_slot_locally?(n)
      return false unless has_parent_slots?
      return parents.any? {|p| p.has_slot_helper(n, v)}
    end
    
  
    def has_slot?(n)
      has_slot_helper(n, Set.new)
    end
    
  
    def get_helper(key, v)
      return nil if v.include?(self)
      v << self
      return @value[key] if has_slot_locally?(key)
      parents.each do |p|
        value = p.get_helper(key, v)
        return value unless value.nil?
      end
      nil
    end


    def get(key)
      get_helper(key, Set.new)
    end


    def remove(key)
      return false unless has_slot_locally?(key)
      @value.delete(key)
      true
    end


    def at_put(key, value)
      return @value[key] = value
    end

    
    def lisp_object?
      true
    end
    
    def type
      :frame
    end
    
    def empty?
      @value.empty?
    end

    def frame?
      true
    end
    
    def length
      return @value.length
    end

    def car
      nil
    end

    def cdr
      nil
    end

    def eq?(other)
      return false unless other.frame?
      return false unless @value.length == other.value.length
      @value.each do |k, v|
        return false unless Lisp::Equivalence.equal_check(other.value[k], v).value
      end
      true
    end

    def to_s
      pairs = @value.collect {|k, v| "#{k.to_s} #{v.to_s}"}
      "{#{pairs.join(' ')}}"
    end
    
  end

end
