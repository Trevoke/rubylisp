module Lisp

  class Exception

    def self.register
      Primitive.register("raise")     {|args, env| Lisp::Exception::raise_impl(args, env) }
      Primitive.register("reraise")   {|args, env| Lisp::Exception::reraise_impl(args, env) }
      Primitive.register("try")       {|args, env| Lisp::Exception::try_impl(args, env) }
      Primitive.register("reraise")   {|args, env| Lisp::Exception::reraise_impl(args, env) }
      Primitive.register("resume")    {|args, env| Lisp::Exception::resume_impl(args, env) }
      Primitive.register("restart")   {|args, env| Lisp::Exception::restart_impl(args, env) }
    end


    def handler_or_nil(handlers, exception_name)
      handlers.each do |handler_pair|
        exceptions = handler_pair.car
        if exceptions.eq(exception_name) || (exceptions.pair? && exceptions.include?(exception_name))
          handler_pair.cdr
        else
          nil
        end
      end
    end

    def self.do_exception(name, with_message, args, env)
      frame = env
      while !frame.nil?
        handlers = frame.value_of(Symbol.named("__handlers__"))
        unless handlers.nil?
          handler = handler_or_nil(handlers, for: name)
          unless handler.nil?
            handler.apply_to_without_evaluating(args, in: frame)
            break
          end
        end
        frame = frame.parent
        if frame.nil?
          exception_message = message.empty? ? "" : ": #{message}"
          return Lisp::Debug.process_error("Unhandled Exception: #{exception_name}#{exception_message}", env)
        end
      end
    end
    
    def self.raise_impl(args, env)
      return Lisp::Debug.process_error("'raise' requires at least one argument.", env) unless args.length > 0
      exception_name = args.car.evaluate(env)
      return Lisp::Debug.process_error("'raise' requires an exception name as it's first argument.", env) unless exception_name.string? || class_name.symbol?

      message = ""
      if args.length > 1
        message = args.cadr.evaluate(env)
        return Lisp::Debug.process_error("The message parameter to 'raise' must be a string.", env) unless message.string?
      end

      handler_args = args.length > 2 ? Lisp::ConsCell.array_to_list(args.cdr.to_a.collect {|a| a.evaluate(env)}) : Lisp::ConsCell.new
    end
  

    def self.try_impl(args, env)
      raw_handlers = args.car
      body = args.cdr
      
      return Lisp::Debug.process_error("Exception handlers must be a list.", env) unless raw_handlers.list?
      return Lisp::Debug.process_error("Exception handlers must be a list of pairs.", env) unless raw_handlers.all? {|h| h.list?}
      return Lisp::Debug.process_error("Exception clause must be a symbol/string or a list of symbol/string.", env) unless raw_handlers.all? do |h|
        ex = h.car
        ex.symbol? || ex.string? || (ex.list? && ex.all? {|h2| h2.symbol? || h2.string?})
      end
      
      array_of_handlers = raw_handlers.to_a.collect do |h|
        ex = h.car
        f = h.cadr.evaluate(env)
      return Lisp::Debug.process_error("Exception handler has to be a function.", env) unless f.function?
        Lisp::ConsCell.cons(ex, f)
      end
      handlers = Lisp::ConsCell.array_to_list(array_of_handlers)
      env.bind_locally(Symbol.named("__handlers__"), handlers)
      
      body.evaluate_each(env)
    end


    def self.reraise_impl(args, env)
    end


    def self.resume_impl(args, env)
    end


    def self.restart_impl(args, env)
    end

    
  end
end

