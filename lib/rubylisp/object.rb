module Lisp

  class NativeObject < Atom

    def self.register
      Primitive.register("wrap-object")  {|args, env| Lisp::NativeObject::wrap_impl(args, env) }
    end

    def self.wrap_impl(args, env)
      return Lisp::Debug.process_error("wrap-object requires 1 argument", env) unless args.length == 1
      raw_val = args.car.evaluate(env)
      val = if raw_val.list?
              raw_val.to_a
            else
              raw_val
            end
      NativeObject.with_value(val)
    end
    
    def self.new_instance_of(c)
      self.new(c.alloc.init)
    end

    def self.with_value(o)
      self.new(o)
    end

    def initialize(o=nil)
      @value = o
    end

    def with_value(&block)
      block.call(@value)
    end
    
    def object?
      true
    end

    def type
      :object
    end

    def native_type
      @value.class
    end

    def to_s
      "<a #{@value.class}: #{@value}>"
    end

    def true?
      @value != nil
    end

    def false?
      @value == nil
    end
    
  end

end
