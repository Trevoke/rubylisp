Gem::Specification.new do |gem|
  gem.name        = 'rubymotionlisp'
  gem.version     = '0.2.0'
  gem.date        = '2014-11-16'
  gem.summary     = "Lisp in RubyMotion"
  gem.description = "An embeddable Lisp as an extension language for RubyMotion"
  gem.authors     = ["Dave Astels"]
  gem.email       = 'dastels@icloud.com'
  gem.files       = Dir.glob('lib/**/*.rb')
  gem.files      << 'README.md'
  gem.require_paths = ['lib']
  gem.homepage    = 'https://bitbucket.org/dastels/rubylisp'
  gem.license     = 'MIT'
end
